#!/usr/bin/env python

import numpy as np
import random as rd
import pandas as pd
from matplotlib import pyplot
from matplotlib.ticker import StrMethodFormatter
from matplotlib import colors

import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
from random import sample

import pickle 

import os,sys,subprocess

def dist(p1, p2):
    return np.sqrt((p1.x - p2.x)**2 + (p1.y - p2.y)**2 + (p1.z - p2.z)**2)

# file description and paths
path = './data/vasc/'
output = './output/scenarios/'

files = os.listdir(path)
files = [s for s in files]

file = path + 'run_4_vasc.csv'
# load files
delimiter = ','
max_columns = max(open(file, 'r'), key = lambda x: x.count(delimiter)).count(delimiter)
df = pd.read_csv(file, header = None, skiprows = 1, names = list(range(0,max_columns+1)), dtype='unicode')

df.dropna(inplace=True) 
#gets last time point
# T_final = df[df[0].str.contains('Time')].index[-1]
T_final = 1
df = df.iloc[T_final:]

# rename columns:
df.columns = ['parameters', 'type', 'vol', 'cycle', 'phase', 'x', 'y', 'z', 'oxygen']
df['parameter'], df['value'] = df['parameters'].str.split(': ', 1).str
df = df[pd.notnull(df['parameters'])]
df.drop(columns='parameters', inplace=True)

#skip the initial data lines 
skip_lines = (df.type.str.contains('type') == True).idxmax()
df = df.loc[skip_lines+1:]

df['r'] = (pd.to_numeric(df.vol)*(3/4)/np.pi)**(1/3)
df[['x','y','z','vol']] = df[['x','y','z','vol']].apply(pd.to_numeric, errors='coerce')

def dist(cell, df):
    print('\r{} still to go...'.format(90383 - cell.name), end = '')
    coords = ['x', 'y', 'z']
    dist = 0
    for c in coords:
        dist += (df[c] - cell[c])**2

    return np.min(np.sqrt(dist))        

celldf = df[df.type == 'CC']
vcdf = df[df.type == 'VC']

fig = plt.figure(dpi=250)

x_w = np.empty(f.shape)
x_w.fill(1/f.shape[0])
bins = np.linspace(0, 250, 40)
plt.hist(f, bins=40, weights=x_w)
plt.axvline(x=f.mean(), color='k', linestyle='--')
plt.text(f.mean()+5,0.02,'Mean = {:1.2g}um'.format(f.mean()) ,rotation=0)

plt.axvline(x=np.percentile(f,95), color='k')
plt.text(np.percentile(f,95)+10,0.01,'95% Percentile = {:1.3g}um\n  (equivalent to 16 cells)'.format(np.percentile(f,95)) ,rotation=0)

plt.xlabel('Distance to nearest VC (um)')
plt.ylabel('Normalised count')
plt.show()


def fill_slice(c_pos):
    cells = ['VC']

    for n in range(1,23):
        near_cells = df[dist(df, c_pos.loc[n])< origin.r]
        if len(near_cells) == 0:
            cells.append('HC')
        else:
            near_cells[near_cells.type == 'CSC'].type == 'CC'
            cells.append(near_cells.sample().type.item())
    return cells


def make_slice(origin):
#     origin = df[df.type == 'VC'].iloc[np.random.randint(0,5000)]

    theta = np.random.random()*2*np.pi
    phi = np.random.random()*2*np.pi

    pos = [origin.x, origin.y, origin.z]
    unit = [np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]

    c_pos = []
    for a in range(23):
        c_pos.append([p + a*2*origin.r*u for p,u in zip(pos,unit)])


    c_pos = pd.DataFrame(c_pos)
    c_pos.columns = ['x','y', 'z']
    
    return c_pos

# #get random selection of vessel cell (this is new origin for cell model)

VCs = df[df.type == 'VC']

s = VCs.iloc[0]

s.name = 0

s.type = 'origin'
s.x = 1
s.y = 0
s.z = 0
VCs.iloc[-1] = s

VCs = VCs[dist(VCs.iloc[-1], VCs) < 220]

VCs.drop(VCs[VCs.x ==0.0].index)
origin = VCs.iloc[np.random.randint(0,len(VCs))]

theta = np.random.random()*2*np.pi
phi = np.random.random()*2*np.pi

pos = [origin.x, origin.y, origin.z]
unit = [np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]
print(unit)
c_pos = []
for a in range(23):
    c_pos.append([p + a*2*origin.r*u for p,u in zip(pos,unit)])
    
c_pos = pd.DataFrame(c_pos)
c_pos.columns = ['x','y', 'z']

c = pd.DataFrame(columns=list(df))
cells = []

for n in range(23):
    near_cells = df[dist(df, c_pos.loc[n])< 2*origin.r]
    print(dist(df[dist(df, c_pos.loc[n])< 2*origin.r], c_pos.loc[n]))
    print(c_pos.loc[n])
    print(near_cells)
    if len(near_cells) == 0:
        HC = {'type':'HC', 'x':c_pos.loc[n].x, 'y':c_pos.loc[n].y, 'z':c_pos.loc[n].z}
        c = c.append(HC, ignore_index=True)
        cells.append('HC')
    else:
        near_cells[near_cells.type == 'CSC'].type == 'CSC'
        c = c.append(near_cells,ignore_index=True)
        cells.append(near_cells.sample().type.item())
c.drop_duplicates(inplace=True)

tissue_sample = []

for n in range(100):
    print(f'\r{100 - n} still to go...', end = '')
    origin = VCs.iloc[np.random.randint(0,len(VCs))]

    c_pos = make_slice(origin)
    cells = fill_slice(c_pos)
    tissue_sample.append(cells)
    
df_ts = pd.DataFrame(tissue_sample)

df_ts = df_ts.replace('VC', 0)
df_ts = df_ts.replace('CC', 1)
df_ts = df_ts.replace('HC', 2)
df_ts = df_ts.replace('CSC', 3)

pickle.dump( df_ts.values.astype(int), open( "CSC_and_CC_new.p", "wb" ) )
# pickle.dump( df_ts.values.astype(int), open( "pure_CC_new.p", "wb" ) )