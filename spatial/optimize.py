#!/usr/bin/env python

import scipy.io as spio
import random
import copy
import time 

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import run_sim_optim

from itertools import repeat
import multiprocessing as mp

class indi:
    def __init__(self ):
        self.gene=[float]*1
        self.fitness=float(0)
        self.name = 0
        
def generate_population(bounds,size=10,indi_size=5):
    population=[]
    for i in range(size):
        population.append(indi())
        population[i].gene[0] = random.uniform(bounds[0],bounds[1])
        if indi_size>1:
            [population[i].gene.append(random.uniform(bounds[2*(j+1)],bounds[2*(j+1)+1])) for j in range(indi_size-1)]
        population[i].fitness = 0
        population[i].name = i
    return population

def selection(population,t_size=2): 
    elite = max(population, key = lambda k: k.fitness)
    parents = []
    for i in range(len(population)):
        candidates = random.sample(population,t_size)
        # requires changing to min/max based on fitness
        parents.append(max(candidates, key = lambda k: k.fitness ))
    return parents, elite

def crossover(parents):
    offspring = []
    offspring.append(parents[0])
    for i in range(len(parents)-1):
        child=indi()
        candid = random.sample(parents,1)
        crossPoint = random.randint(1,len(parents[0].gene)-1)
        child.gene[0] = parents[i].gene[0]
        for j in range(1,len(parents[0].gene)):
            tres = parents[i].gene[j] if j<crossPoint else candid[0].gene[j]
            child.gene.append(tres)
        child.fitness = 0
        offspring.append(child)
    return offspring

def mutate(offspring,bounds,m_rate=0.3,percentMut=0.15):
    final_offspring = copy.deepcopy(offspring)
    for individual in final_offspring:
        if random.random()<m_rate:
            mutPoint=random.randint(0,len(final_offspring[0].gene)-1)
            individual.gene[mutPoint] *=random.uniform(1-percentMut,1+percentMut)
            if individual.gene[mutPoint] > bounds[(2*mutPoint)+1]:
                individual.gene[mutPoint] = bounds[(2*mutPoint)+1]
            elif individual.gene[mutPoint] < bounds[(2*mutPoint)] :
                individual.gene[mutPoint] = bounds[(2*mutPoint)]
    return final_offspring

def replace(population,f_offspring):
    new_pop = []
    for i in range(len(f_offspring)):
        # < for max, > for min
        if population[i].fitness < f_offspring[i].fitness:
            f_offspring[i].name = i
            new_pop.append(f_offspring[i])
        else:
            population[i].name =i
            new_pop.append(population[i]) 
    return new_pop

def evaluate_testing(population):
    for individual in population:
        individual.fitness = sum(individual.gene)
    return population

def evaluate_function(population, N_runs=1,n=0):
    if N_runs >1:
        tmp_population = [makeInput(p.gene) for p in population for r in range(N_runs) ]
        text_file = open("Output{}.txt".format(n), "w")
        text_file.write(f"\n\n\n\n{tmp_population}")
        text_file.close()
        with mp.Pool(mp.cpu_count()) as p:
            tmp_population = p.map(run_sim_optim.run_sim, tmp_population)
        for i, individual in enumerate(population):
            f = [p for p in tmp_population[N_runs*i:N_runs*i + N_runs]]
            individual.fitness = np.mean(f)
            print('fitness of gene is %s\n' % individual.fitness)
        return population

def makeInput(inputParams):
    # Run STEPS with inputParams    
    NP0t = inputParams[2]/(48*3600)
    NP1t = inputParams[6]/(48*3600)
    alpha = 2.56e-8 # = Molar mass(543.2g/mol)*Tumour volume (5mm^3)/(Mouse weight(20g)*Ncells(22)*CellLength^3(10um*10um*10um)*Avogadro(6e23))
    beta = 6022140 # = Potency(10uM)*CellLength^2 (10um*10um)*Avogadro(6e23)
    NP0c = beta/inputParams[3]
    NP1c = beta/inputParams[7]
    # Fitness constraint = Injected Dose = NP0*E*alpha = (beta*48*3600)*NP0t/NPc
    max_dose = 55

    constraint1 = inputParams[2]*inputParams[3]*alpha 
    constraint1 = constraint1/max_dose

    constraint2 = inputParams[6]*inputParams[7]*alpha 
    constraint2 = constraint2/max_dose

    constraint = constraint1 + constraint2
    # If ID is too high then don't need to evaluate
    if (inputParams[2]*inputParams[3]*alpha > max_dose) | (inputParams[6]*inputParams[7]*alpha > max_dose):
        return (0, 0, 0)

    print(f'ID for NP0 = {inputParams[2]*inputParams[3]*alpha:3.5f} mg/kg')
    print(f'ID for NP1 = {inputParams[6]*inputParams[7]*alpha:3.5f} mg/kg')
    VCParam = {'P0':{"k_a" :0, "D" :inputParams[0],"NP0" :NP0t}
                 ,'P1':{"k_a" :0, "D" :inputParams[4],"NP0" :NP1t}}
    CCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0], "NP_max" : NP0c}
                 ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4], "NP_max" : 10000000}}
    CSCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0], "NP_max" : 10000000}
                 ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4], "NP_max" : NP1c}}
    HCParam = {'P0':{"k_a" : 0,"k_d" :1e-4,"D" :inputParams[0]}
                 ,'P1':{"k_a" :0,"k_d" :1e-4,"D" :inputParams[4]}}
    
    stepsParams = {"cell":{"VC": VCParam, "CC": CCParam, "HC": HCParam, "CSC": CSCParam}}
    
    sim_opt, cell_data =  run_sim_optim.load_settings(stepsParams)
    return (sim_opt, cell_data, constraint)

def parallelSTEPS(individual):
    # Update individual fitness
    individual.fitness = run_sim_optim.run_sim(sim_opts)
    # Return individual
    return individual

def main(n=0):
    # Set parameters of GA
    indi_size = 8
    N_runs = 5
    # Pairs of lower and higher boundaries for alleles
    Dmin = 1e-14
    Dmax = 1e-10
    kamin = 1e3
    kamax = 1e6
    NP0min = 1.32e4
    NP0max = 1.32e7
    Emin = 1e1
    Emax = 1e4

    bounds = [Dmin, Dmax, kamin, kamax, NP0min, NP0max, Emin, Emax, Dmin, Dmax, kamin, kamax, NP0min, NP0max, Emin, Emax] 

    pop_size = 50 
    generations = 500
    # evaluate_function # Change into the evaluating function
    
    # Initialize temporary list for saving
    parents = []
    offspring = []
    f_offspring=[]
    history=[]      

    print(f'Population generated')
    print(f'Npop = {pop_size}, generations = {generations}, N_runs = {N_runs}')
    # Generate initial population and evaluate it
    population = generate_population(size=pop_size,indi_size=indi_size,bounds=bounds)
    population = evaluate_function(population, N_runs=N_runs,n=n)
    for individual in population:
        history.append([individual.gene,individual.fitness])

    print(f'Beginning evolution')

    # Run the generations of evolution
    for gen in range(generations):
        print(f'\n------- generation {gen}-------\n')
        # Select the fittest parents (& one elite)
        parents, elite = selection(population)
        print(f'elite has fitness of {elite.fitness}')
        
        # Produce the offspring
        offspring = crossover(parents)
        
        # Mutate the offspring
        f_offspring = mutate(offspring,bounds)
           
        # Replace last population with the elite
        #f_offspring[-1] = elite

        # Evaluate the new population and do replacement on the initial one
        f_offspring = evaluate_function(f_offspring, N_runs=N_runs,n=n)
        print(f'elite has updated fitness of {f_offspring[-1].fitness}')

        population = replace(population,f_offspring)
        
        for individual in population:
            history.append([individual.gene,individual.fitness])
    # Finalize execution and save results
    print('End of evolution')
    output = { 'pop'+str(i) : history[i] for i in range(len(history)) }
    spio.savemat('output_evolution{}.mat'.format(n),output)
# =============================================================================
    df = pd.DataFrame(history)
    plotingArray=[df[1][k:k+pop_size].min() for k in range(0,pop_size*generations+1,pop_size)]
    fig, ax = plt.subplots()
    ax.plot(range(generations+1),plotingArray)
    ax.set(xlabel='Generation', ylabel='Fitness value', title='Evolution of best solution')
    ax.grid()
    fig.savefig('result{}.eps'.format(n), format='eps')
    # idxmax,max for max, idxmin,min for min
    bestSolution=df[0][df[1].idxmax()]
    print(f'Solution after evolution: \n {bestSolution}')
    print(f'With fitness: {df[1].max()}')
# =============================================================================

# elitism includes    

if __name__ == "__main__":

    tic = time.perf_counter()
    main(151)
    toc = time.perf_counter()
    print(f"Runtime in {toc - tic:0.4f} seconds")
