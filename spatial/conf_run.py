import numpy as np

import setup as s
import run_sim_optim as rs
import matplotlib.pyplot as plt

from itertools import product  

import time
import pickle 

ka = [1e6]
D = [1e-10, 1e-12, 1e-14]
NP0 = 1e3
T = 3600*12
mesh = []

opts = ["10","8","5","1"]

dist = ["unif", "point", "boundary"]

t1 = time.time() 

for p in list(product(ka,D,opts,dist)):
    
    k, D, o, d = p
    print(p)
    CCParam = {'P0':{"k_a" :k,"D" :D, "NP0":NP0, "T":T}}

    stepsParams = {"category":{"opt":{"mesh":f"{o}um", "dist": f"{d}"}},
                   "cell":{"CC": CCParam}}

    sim, cell = rs.load_settings(stepsParams)
    sim, cell = rs.load_settings(stepsParams)
    f = 0
    input_data = sim,cell,f
    (data, m, f) = rs.run_sim(input_data)
    t2 = time.time() 
    file = f"output/run_k_{k}_D_{D}_o_{o}_mesh_{d}_time_{t2 - t1:0.2f}s.p"

    pickle.dump(data,open(file, 'wb'))

    print(file + " completed\n" )