import steps.utilities.meshio as smeshio
import steps.utilities.meshctrl as meshctrl
import steps.solver as solvmod
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng

import numpy as np
import mmap
import copy
import re

import setup, plotting_3d

def model(tissue):
    """ initialises model using tissue array """

    mdl = smodel.Model()
    unique_p = []

    for cell in tissue:
        [unique_p.append(p) for p in cell.prtcl_names if p not in unique_p]
    
    NP = []
    NPi = []
    # Create particles and corresponding species
    for p in unique_p:
        # Free NPs
        NP.append(smodel.Spec('N{}'.format(p), mdl))
        # internalised NPs
        NPi.append(smodel.Spec('N{}i'.format(p), mdl))
        
    # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)
    # complexes state: NPs bound to a cell receptor
    NPR = smodel.Spec('NPR', mdl)
    d = {}
    rxn_ = {}

    # Create volume for diffusion of NPs
    vsys1 = smodel.Volsys('tissue', mdl)
    vsys2 = smodel.Volsys('vessel', mdl)

    dfsn_ = {} 
    # Loop where cell and particle properties are connected to reactions   
    for n,cell in enumerate(tissue):   
        for p_idx, p in enumerate(unique_p):
            tag = str(n) + p
            prtcl = getattr(cell, p)
            d["memb{}".format(tag)] = smodel.Surfsys("memb{}".format(tag), mdl)
            k_diff = prtcl['D']
             # binding reactions:
            if 'k_a' in prtcl:
                k_bind = prtcl['k_a']
                k_unbind = prtcl['k_d']
                k_intern = prtcl['k_i']
                rxn_["bind_{}".format(tag)] = smodel.SReac("bind_{}".format(tag), d["memb{}".format(tag)], 
                    olhs=[NP[p_idx]], slhs =[R], srhs=[NPR], kcst=k_bind)
                rxn_["unbind_{}".format(tag)] = smodel.SReac("unbind_{}".format(tag), d["memb{}".format(tag)], 
                    slhs=[NPR], orhs=[NP[p_idx]], srhs= [R], kcst=k_unbind)
                rxn_["intern_{}".format(tag)] = smodel.SReac("intern_{}".format(tag), d["memb{}".format(tag)], 
                    slhs=[NPR], irhs=[NPi[p_idx]],srhs= [R], kcst=k_intern)

            # Diffusion 
            dfsn1 = smodel.Diff('dfsn{}'.format(2*n +1), vsys1, NP[p_idx], dcst = k_diff)    
            dfsn2 = smodel.Diff('dfsn{}'.format(2*n), vsys2, NP[p_idx], dcst = k_diff)    

    return mdl


def geom(tissue, sim_opt):
    unique_p = []
    inc_cells = bool(int(sim_opt['general'].N_cells))
    for cell in tissue:
        [unique_p.append(p) for p in cell.prtcl_names if p not in unique_p]

    print('parameters: mesh_name = {0}_{1}'.format(sim_opt['opt'].tissue,sim_opt['opt'].mesh))
    
    meshf = 'meshes/{0}/{0}_{1}.inp'.format(sim_opt['opt'].tissue,sim_opt['opt'].mesh)
    #scale is 1um (domain size is 40um)

    mesh, nodeproxy, tetproxy, triproxy  = smeshio.importAbaqus(meshf, 1e-6)
    if bool(int(sim_opt['opt'].savemesh)):
        smeshio.saveMesh(sim_opt['opt'].mesh, mesh)

    tet_groups = tetproxy.blocksToGroups()
    f = open(meshf)
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

    vol_tag = []
    type_tags = []
    while s.find(b'Volume') != -1:
        m = re.search(b'Volume', s)
        if len(vol_tag) < 30:
            volume = s[m.start(): m.end() + 3]
        else:
            volume = s[m.start(): m.end() + 4]
        vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
        s = s[m.end():-1]

    domain_tets = tet_groups["Volume186"]# % 186 is specified in the .geo file
    tissue = sgeom.TmComp('tissue', mesh, domain_tets)
    tissue.addVolsys('tissue')
    vol_tag.remove('186')
    tissue_vessel_db = []
    type_tags = [t.split("_") for t in type_tags]
    print(f'Successfully found {len(vol_tag)} cells')
    if inc_cells:
        msh_ = {}
        memb_ = {}
        cell_ = {}
        for n,vtag in enumerate(vol_tag): 
            for p_idx, p in enumerate(unique_p):
                tag = str(n) + p
                msh_['c_tets{}'.format(tag)] = tet_groups["Volume%s" % vtag]
                msh_['memb_tets{0}'.format(tag)] = meshctrl.findOverlapTris(mesh, domain_tets, msh_['c_tets{}'.format(tag)])
                cell_['cell{}'.format(tag)] = sgeom.TmComp('cell{}'.format(tag), mesh, msh_['c_tets{}'.format(tag)])
                cell_['memb{}'.format(tag)] = sgeom.TmPatch('memb{}'.format(tag), mesh, msh_['memb_tets{}'.format(tag)], \
                    icomp = cell_['cell{}'.format(tag)], ocomp = tissue)
                cell_['memb{}'.format(tag)].addSurfsys('memb{}'.format(tag))

    geom = [mesh, domain_tets, type_tags]   
    return geom

def boundary_tets(geom):

    ntets = geom[0].countTets()
    boundary_tets = []
    boundary_tris = set()

    z_max = geom[0].getBoundMax()[2]
    eps = 1e-6
    for t in range(ntets):
        # Fetch the z coordinate of the barycenter
        barycz = geom[0].getTetBarycenter(t)[2]
        # Fetch the triangle indices of the tetrahedron, a tuple of length 4:
        tris = geom[0].getTetTriNeighb(t)
        if barycz > z_max - eps :
            boundary_tets.append(t)
            boundary_tris.add(tris[0])
            boundary_tris.add(tris[1])
            boundary_tris.add(tris[2])
            boundary_tris.add(tris[3])
    return boundary_tets

def point_tets(geom):

    ntets = geom[0].countTets()
    point_tets = []
    point_tris = set()

    x_max = geom[0].getBoundMax()[0]/2
    y_max = geom[0].getBoundMax()[1]/2
    z_max = geom[0].getBoundMax()[2]/2
    eps = 1e-6
    for t in range(ntets):
        # Fetch the z coordinate of the barycenter
        barycz = geom[0].getTetBarycenter(t)[2]
        # Fetch the triangle indices of the tetrahedron, a tuple of length 4:
        tris = geom[0].getTetTriNeighb(t)
        if barycz > x_max - eps :
            if barycz > y_max - eps :
                if barycz > z_max - eps :
                    point_tets.append(t)
                    point_tris.add(tris[0])
                    point_tris.add(tris[1])
                    point_tris.add(tris[2])
                    point_tris.add(tris[3])
    return point_tets

def solver(mdl, geom, tissue, sim_opt):

    unique_p = []
    for cell in tissue:
            [unique_p.append(p) for p in cell.prtcl_names if p not in unique_p]
    inc_cells = bool(int(sim_opt['general'].N_cells))
    
    treated_tissue = copy.deepcopy(tissue) 
    reactants = ['NP','NPi', 'NPR', 'R']
    NT = int(sim_opt['general'].NT)
    t_final = int(sim_opt['general'].t_final)
    NP0 = float(tissue[0].P0['NP0'])
    NR = float(tissue[0].NR)
    print('NP0=',NP0)

    N_cells = int(sim_opt['general'].N_cells)
    mesh = geom[0]
    domain_tets = geom[1]
    type_tags = geom[2]

    # Create rnadom number generator object
    rng = srng.create('mt19937', 512)
    rng.initialize(np.random.randint(1, 10000))

    # Create solver object
    sim = solvmod.Tetexact(mdl, mesh, rng)

    # sim.reset()
    tpnt = np.linspace(0.0, t_final, NT)
    ntpnts = tpnt.shape[0]

    # Create the simulation data structures
    resi = np.zeros((ntpnts,N_cells,  len(reactants)))

    c_type = ''

    for n,cell in enumerate(treated_tissue):
        for p in cell.prtcl_names:
            tag = str(n) + p
            prtcl = getattr(cell, p)
            if sim_opt['opt'].dist == 'unif':
                if  prtcl['T'] == 1:
                    print('Release of %g NPs' % prtcl['NP0'])
                    sim.setCompCount("tissue", 'N{}'.format(p), prtcl['NP0'])
                else:    
                    sim.setCompCount("tissue", 'N{}'.format(p), prtcl['NP0']/NT)
            else:
                if sim_opt['opt'].dist == 'point':
                    tetX = geom[0].findTetByPoint([0,0,0])
                    if  prtcl['T'] == 1:
                        sim.setTetCount(tetX, 'NP0', NP0)
                    else:    
                        sim.setTetCount(tetX, 'NP0', prtcl['NP0']/NT)
                else:
                    tetX = boundary_tets(geom)
                    for t in tetX:
                        sim.setTetCount(t, 'NP0', NP0/len(tetX))    
                        if  prtcl['T'] == 1:
                            sim.setTetCount(t, 'NP0', NP0/len(tetX))
                        else:    
                            sim.setTetCount(t, 'NP0', prtcl['NP0']/NT/len(tetX))            
            if hasattr(cell,'NR') and inc_cells:
                sim.setPatchCount('memb{}'.format(tag), 'R', NR)

    if bool(int(sim_opt['opt'].visualise)):
        plotting_3d.check_plot(sim, geom, tissue, sim_opt)
        return 'Simulation complete'

    else:
        if bool(int(sim_opt['opt'].print_progress)):
            setup.printProgressBar(0, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)                
        for t in range(NT):
            sim.run(tpnt[t])
            if bool(int(sim_opt['opt'].print_progress)):
                setup.printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)
            n = 0
            for nc,cell in enumerate(treated_tissue):
                for p_idx, p in enumerate(cell.prtcl_names):
                    tag = str(nc) + p
                    prtcl = getattr(cell, p)
                    if hasattr(cell,'NR'):
                        resi[t, n, 0] = sim.getCompCount('tissue', 'N{}'.format(p))                           
                        if inc_cells:
                            resi[t, n, 1] = sim.getPatchCount("memb{}".format(tag), 'R')                                    
                            resi[t, n, 2] = sim.getPatchCount("memb{}".format(tag), 'NPR')                                    
                            resi[t, n, 3] = sim.getCompCount("cell{}".format(tag), 'N{}i'.format(p))                                    
                    if 'T' in prtcl:
                        if (tpnt[t] > prtcl['T']) or (prtcl['T'] == 1):
                            sim.setCompClamped("tissue", 'N{}'.format(p), False)
                        else:
                            # print('constant_release')
                            currentNP = sim.getCompCount("tissue", 'N{}'.format(p)) 
                            sim.setCompCount("tissue", 'N{}'.format(p), currentNP + prtcl['NP0']*(t_final/prtcl['T'])/NT)
                            currentNP = sim.getCompCount("tissue", 'N{}'.format(p)) 
                        
                    if ('NP_max' in prtcl) and (resi[t, n, 3] > prtcl['NP_max']):
                        cell.type = 'dead'
                    n += 1
        return treated_tissue, resi   
