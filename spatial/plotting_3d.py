import pyqtgraph as pg
import steps.visual as visual
import pylab

def plot_binned(t_idx, bin_n = 100, solver='unknown'):
    if (t_idx > tpnts.size):
        print("Time index is out of range.")
        return

    # Create structure to record z-position of tetrahedron
    z_tets = np.zeros(ntets)

    zbound_min = mesh.getBoundMin()[1]

    # Now find the distance of the centre of the tets to the Z lower face
    for i in range(ntets):
        baryc = mesh.getTetBarycenter(i)
        z = baryc[1] - zbound_min
        # Convert to microns and save
        z_tets[i] = z*1.0e6

    # Find the maximum and minimum z of all tetrahedrons
    z_max = z_tets.max()
    z_min = z_tets.min()

    # Set up the bin structures, recording the individual bin volumes
    z_seg = (z_max-z_min)/bin_n
    bin_mins = np.zeros(bin_n+1)
    z_tets_binned = np.zeros(bin_n)
    bin_vols = np.zeros(bin_n)

    # Now sort the counts into bins for species 'X'
    z = z_min
    for b in range(bin_n + 1):
        bin_mins[b] = z
        if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
        z+=z_seg
    bin_counts = [None]*bin_n
    for i in range(bin_n): bin_counts[i] = []
    for i in range((resX[t_idx].size)):
        i_z = z_tets[i]
        for b in range(bin_n):
            if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
                bin_counts[b].append(resX[t_idx][i])
                bin_vols[b]+=sim.getTetVol(i)
                break

    # Convert to concentration in arbitrary units
    bin_concs = np.zeros(bin_n)
    for c in range(bin_n):
        for d in range(bin_counts[c].__len__()):
            bin_concs[c] += bin_counts[c][d]
        bin_concs[c]/=(bin_vols[c]*1.0e18)

    t = tpnts[t_idx]

    # Plot the data
    pylab.scatter(z_tets_binned, bin_concs, label = 'X', color = 'blue')

    # Repeat the process for species 'Y'- separate from 'X' for clarity:
    z = z_min
    for b in range(bin_n + 1):
        bin_mins[b] = z
        if (b!=bin_n): z_tets_binned[b] = z + z_seg/2.0
        z+=z_seg
    bin_counts = [None]*bin_n
    for i in range(bin_n): bin_counts[i] = []
    for i in range((resY[t_idx].size)):
        i_z = z_tets[i]
        for b in range(bin_n):
            if(i_z>=bin_mins[b] and i_z<bin_mins[b+1]):
                bin_counts[b].append(resY[t_idx][i])
                break
    bin_concs = np.zeros(bin_n)
    for c in range(bin_n):
        for d in range(bin_counts[c].__len__()):
            bin_concs[c] += bin_counts[c][d]
        bin_concs[c]/=(bin_vols[c]*1.0e18)

    pylab.scatter(z_tets_binned, bin_concs, label = 'Y', color = 'red')

    pylab.xlabel('Z axis (microns)', fontsize=16)
    pylab.ylabel('Bin concentration (N/m^3)', fontsize=16)
    pylab.ylim(0)
    pylab.xlim(0, 2)
    pylab.legend(numpoints=1)
    pylab.title('Simulation with '+ solver)
    pylab.show()

def check_plot(sim, geom, tissue, sim_opt):

    NT = int(sim_opt['general'].NT)
    t_final = int(sim_opt['general'].t_final)
    NP0 = float(tissue[0].P0['NP0'])
    print('NP0=',NP0)
    NR = float(tissue[0].NR)
    scenario = sim_opt['opt'].scenario
    N_cells = int(sim_opt['general'].N_cells)
    mesh = geom[0]
    domain_tets = geom[1]
    type_tags = geom[2]
    
    # Visualization initialization
    app = pg.mkQApp()
    print('--- visualising mesh ---')
    plots = visual.PlotDisplay("Simple model", size = (600, 400))

    # Create simulation displays < only consider full view
    full_display = visual.SimDisplay("Full View")

    # Create Plots
    pen = pg.mkPen(color=(255,255,255), width=2)

    # p = plots.addCompSpecPlot("<span style='font-size: 16pt'>cell", sim, 'cell{}P0'.format(N_cells - 1), "NPi", data_size = 1000, measure = "count", pen=(255, 0.647 * 255, 0))
    p = plots.addCompSpecPlot("<span style='font-size: 16pt'>tissue", sim, 'tissue', "NP0", data_size = 1000, measure = "count", pen=(255, 0.647 * 255, 0))
    p.getAxis('left').setPen(pen)
    p.getAxis('bottom').setPen(pen)
    p.showGrid(x=True, y=True)
    labelStyle = {'color': '#ffffff', 'font-size': '16px'}
    p.setLabel('bottom', 'Time', 's', **labelStyle)

    if N_cells > 1:
        # Create static mesh components
        vessel_view = visual.VisualCompMesh("tissue", full_display, mesh, "tissue", color = [0.500, 0.000, 0.000, 0.2])
        d_ = {}
        for n in range(N_cells):
            visual.VisualCompMesh('cell{0}P0'.format(n), full_display, mesh, 'cell{0}P0'.format(n), color = [0.0, 0.700, 0.0, 0.2])
        NP_vesel = visual.VisualTetsSpec("tissue", full_display, mesh, sim,domain_tets, "NP0", spec_size = 1)

    elif sim_opt['opt'].tissue == "no_cell":
        vessel_view = visual.VisualCompMesh("tissue", full_display, mesh, "tissue", color = [0.500, 0.000, 0.000, 0.2])
        NP_vesel = visual.VisualTetsSpec("tissue", full_display, mesh, sim,domain_tets, "NP0", spec_size = 1)

    else:
        # Create static mesh components
        vessel_view = visual.VisualCompMesh("tissue", full_display, mesh, "tissue", color = [0.500, 0.000, 0.000, 0.2])
        visual.VisualCompMesh('cell0P0', full_display, mesh, 'cell0P0', color = [0.0, 0.700, 0.0, 0.2])

        NP_vesel = visual.VisualTetsSpec("tissue", full_display, mesh, sim,domain_tets, "NP0", spec_size = 0.2)


    # Add simulation and displays to control
    x = visual.SimControl([sim], [full_display],[plots], end_time= t_final, upd_interval =  NT)

    # Enter visualization loop
    app.exec_()