import steps.utilities.meshio as smeshio
import steps.utilities.meshctrl as meshctrl
import steps.solver as solvmod
import steps.model as smodel
import steps.geom as sgeom
import steps.rng as srng

import numpy as np
import mmap
import copy
import re

import misc, plotting_3d

def model(tissue):
    """ initialises model using tissue array """

    mdl = smodel.Model()
    unique_p = []
    for cell in tissue:
        [unique_p.append(p) for p in cell.prtcl_names if p not in unique_p]
    
    NP = []
    NPi = []
    NPR = []
    # Create particles and corresponding species
    for p in unique_p:
        # Free NPs
        NP.append(smodel.Spec('N{}'.format(p), mdl))
        # internalised NPs
        NPi.append(smodel.Spec('N{}i'.format(p), mdl))
        # complexes state: NPs bound to a cell receptor
        # NPR.append(smodel.Spec('N{}R'.format(p), mdl))
        
        # receptor state: 'naive' state (no bound NPs)
    R = smodel.Spec('R', mdl)
    NPR = smodel.Spec('NPR', mdl)
    d = {}
    rxn_ = {}
    dfsn_ = {} 
    # Lpop where cell and particle properties are connected to reactions   
    for n,cell in enumerate(tissue):    
        for p_idx, p in enumerate(unique_p):
            tag = str(n) + p
            prtcl = getattr(cell, p)
            d["cell{}".format(tag)] = smodel.Volsys("cell{}".format(tag), mdl)
            k_diff = prtcl['D']

             # binding reactions:
            if 'k_a' in prtcl:
                k_bind = prtcl['k_a']
                k_unbind = prtcl['k_d']
                k_intern = prtcl['k_i']
                rxn_["bind_{}".format(tag)] = smodel.Reac("bind_{}".format(tag), d["cell{}".format(tag)], 
                    lhs=[NP[p_idx], R], rhs=[NPR], kcst=k_bind)
                rxn_["unbind_{}".format(tag)] = smodel.Reac("unbind_{}".format(tag), d["cell{}".format(tag)], 
                    lhs=[NPR], rhs=[NP[p_idx], R], kcst=k_unbind)
                rxn_["intern_{}".format(tag)] = smodel.Reac("intern_{}".format(tag), d["cell{}".format(tag)], 
                    lhs=[NPR], rhs=[NPi[p_idx], R], kcst=k_intern)

            # Diffusion 
            dfsn1 = smodel.Diff('dfsn1', d["cell{}".format(tag)], NP[p_idx], dcst = k_diff)    
            dfsn2 = smodel.Diff('dfsn2', d["cell{}".format(tag)], NPR, dcst =k_diff)
            dfsn3 = smodel.Diff('dfsn3', d["cell{}".format(tag)], R, dcst = k_diff)
            dfsn4 = smodel.Diff('dfsn4', d["cell{}".format(tag)], NPi[p_idx], dcst = k_diff)

    return mdl

def boundary_tets(geom):

    ntets = geom[0].countTets()
    boundary_tets = []
    boundary_tris = set()

    z_max = geom[0].getBoundMax()[2]

    # print(geom[0].getBoundMax()[0])
    # print(geom[0].getBoundMax()[1])
    # print(geom[0].getBoundMax()[2])
    # print(geom[0].getBoundMin()[0])
    # print(geom[0].getBoundMin()[1])
    # print(geom[0].getBoundMin()[2])

    eps = 0.5e-6
    for t in range(ntets):
        # Fetch the z coordinate of the barycenter
        barycz = geom[0].getTetBarycenter(t)[2]
        # Fetch the triangle indices of the tetrahedron, a tuple of length 4:
        tris = geom[0].getTetTriNeighb(t)
        if barycz > z_max - eps :
            boundary_tets.append(t)
            boundary_tris.add(tris[0])
            boundary_tris.add(tris[1])
            boundary_tris.add(tris[2])
            boundary_tris.add(tris[3])

    print(boundary_tets)

    return boundary_tets

def geom(tissue, sim_opt):
    print('parameters: mesh_name = %s' %  sim_opt['opt'].mesh)
    cell = tissue[0]
    meshf = 'meshes/{0}_{1}.inp'.format(sim_opt['opt'].scenario, sim_opt['opt'].mesh)
    #scale is 1um (domain size is 10um)

    mesh, nodeproxy, tetproxy, triproxy  = smeshio.importAbaqus(meshf, 1e-6)
    if sim_opt['opt'].savemesh == 1:
        smeshio.saveMesh(sim_opt['opt'].mesh, mesh)

    tet_groups = tetproxy.blocksToGroups()
    f = open(meshf)
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)

    vol_tag = []
    type_tags = []
    while s.find(b'Volume') != -1:
        m = re.search(b'Volume', s)
        if len(vol_tag) < 30:
            volume = s[m.start(): m.end() + 3]
        else:
            volume = s[m.start(): m.end() + 4]
        vol_tag.append(''.join([n for n in str(volume) if n.isdigit()]))
        s = s[m.end():-1]

    domain_tets = tet_groups["Volume186"]# % 186 is specified in the .geo file
    tissue = sgeom.TmComp('cell0P0', mesh, domain_tets)
    tissue.addVolsys('cell0P0')
    vol_tag.remove('186')
    mesh_output = [mesh, domain_tets, type_tags]
    return mesh_output

def solver(mdl, geom, tissue, sim_opt):

    treated_tissue = copy.deepcopy(tissue) 
    reactants = ['NP','NPi', 'NPR', 'R']
    NT = int(sim_opt['general'].NT)
    t_final = int(sim_opt['general'].t_final)
    NP0 = float(tissue[0].P0['NP0'])
    print('NP0=',NP0)
    NR = float(tissue[0].NR)

    N_cells = int(sim_opt['general'].N_cells)
    mesh = geom[0]
    domain_tets = geom[1]
    type_tags = geom[2]

    # Create rnadom number generator object
    rng = srng.create('mt19937', 512)
    rng.initialize(np.random.randint(1, 10000))

    # Create solver object
    sim = solvmod.Tetexact(mdl, mesh, rng)

    sim.reset()
    tpnt = np.linspace(0.0, t_final, NT)
    ntpnts = tpnt.shape[0]

    # Create the simulation data structures
    ntets = mesh.countTets()
    resi = np.zeros((ntpnts,1,  len(reactants)))

    c_type = ''
    if sim_opt['opt'].unif == '1':
        sim.setCompCount('cell0P0', 'NP0', NP0)
    else:
        tetX = boundary_tets(geom)
        for n in tetX:
            sim.setTetCount(n, 'NP0', NP0/len(tetX)) 

    sim.setCompCount('cell0P0', 'R', NR)

    if sim_opt['opt'].visualise == '1':
        plotting_3d.plotres(sim, geom, tissue, sim_opt)
        return 'Simulation complete'

    else:
        misc.printProgressBar(0, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)                
        for t in range(NT):
            sim.run(tpnt[t])
            misc.printProgressBar(t, NT, prefix = 'Progress:', suffix = 'Complete', length = 40)
            n = 0
            for nc,cell in enumerate(treated_tissue):
                for p_idx, p in enumerate(cell.prtcl_names):
                    tag = str(nc) + p
                    prtcl = getattr(cell, p)
                    if hasattr(cell,'NR'):
                        resi[t, n, 0] = sim.getCompCount("cell{}".format(tag), 'N{}'.format(p))   
                        resi[t, n, 1] = sim.getCompCount("cell{}".format(tag), 'R')                                    
                        resi[t, n, 2] = sim.getCompCount("cell{}".format(tag), 'NPR')                                    
                        resi[t, n, 3] = sim.getCompCount("cell{}".format(tag), 'N{}i'.format(p))                                    
                    else: 
                        resi[t, n, 0] = sim.getCompCount("cell{}".format(tag), 'N{}'.format(p))                                    
                    if 'T' in prtcl:
                        if (tpnt[t] > prtcl['T']) or (prtcl['T'] == 1):
                            sim.setCompClamped("cell{}".format(tag), 'N{}'.format(p), False)
                    if ('NP_max' in prtcl) and (resi[t, n, 3] > prtcl['NP_max']):
                        cell.type = 'dead'
                    n += 1
        return treated_tissue, resi   
