import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle, sys
import os.path, csv
import json
import random

import setup

import argparse
from itertools import repeat
from multiprocessing import Pool, TimeoutError, cpu_count

def save_output(resi, strings):
    sim_str, time_take, filename = strings[0], strings[1], strings[2]
    file_name = 'result_%s_time_%.2g' % (sim_str, time_taken)

    with open('output/'+ sim_str+'/'+ file_name + '.pickle', 'wb') as f:
        pickle.dump(resi, f)
    return 0

def load_settings(*args):

    path = "wmxd_chamber_optim_config.xml"
    sim_opt = setup.get_sim_data(path)
    if len(args) > 0: 
        data = args[0]
        # print("Input data is %s" % data)
        setup.update_config(data, path)
        
    cell_data = setup.get_cell_data(path)
    return sim_opt, cell_data

def main(input_data):

    output_df = pd.DataFrame(columns=['killed(CC)','NCC', 'killed(CSC)', 'NCSC', 'NHC', 'NVC'])
    sim_opt, cell_data, fitness_constraint = input_data
    for i in range(100):
        if sim_opt == 0:
            return 0
        path = "wmxd_chamber_optim_config.xml"
        # prepare tissue data
        if sim_opt['opt'].tissue == 'test':
            tissue = np.array([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
            tissue_map  = {0: 'VC', 1 : 'CC'}
        elif sim_opt['opt'].tissue == 'CSC_and_CC.p':
            tissue_map  = {0: 'VC', 1 : 'CC', 2: 'HC', 3: 'CSC'}
        else:
            tissue_map  = {0: 'VC', 1 : 'CC', 2: 'HC'}

        section =i
        tissue = pickle.load( open( './output/scenarios/' +  sim_opt['opt'].tissue, "rb" ) )
        tissue = tissue[section,:] 

        # prepare cell data
        data = cell_data

        for n in range(len(tissue_map)):
            data[n]['N_cell'] = sum((tissue == n)*1)
        
        cells = setup.make_cells(data)

        tissue = setup.make_tissue(tissue, cells, tissue_map)

        # prepare simulation
        scenario = __import__(sim_opt['opt'].scenario)
        mdl = scenario.model(tissue)
        geom = scenario.geom(tissue, sim_opt)

        # run simulation
        start = time.time()
        treated_tissue, resi = scenario.solver(mdl, geom, tissue, sim_opt)
        end = time.time()
        time_taken = end - start
        
        # plotting:
        if sim_opt['opt'].visualise == '1':
            scenario.check_plot(tissue, treated_tissue,resi)
        sim_str = sim_opt['general'].output_file

        fitness, message, kills = setup.calc_fitness(tissue,treated_tissue, fitness_constraint)
        tmp = pd.Series(kills, index = output_df.columns)
        output_df = output_df.append(tmp, ignore_index = True)
        output_df.to_csv('CC_soln.csv')

        print(f'\r{message}\nTissue section {section} w/ fitness {fitness:g}')

    return "testing finished"

if __name__ == "__main__":
    inputParams = [9.838041007410792e-11, 70132.70903975719, 60408.023713651244, 5020.706360301366]

    # Run STEPS with inputParams    
    if len(inputParams) > 4:
        print(f'total NPs should be {inputParams[2] + inputParams[6]}')
        NP0t = inputParams[2]
        NP1t = inputParams[6]
        alpha = 1e6*2.56e-14 # = Molar mass(543.2g/mol)*Tumour volume (5mm^3)/(Mouse weight(20g)*Ncells(22)*CellLength^3(10um*10um*10um)*Avogadro(6e23))
        beta = 6022140 # = Potency(10uM)*CellLength^2 (10um*10um)*Avogadro(6e23)
        NP0c = beta/inputParams[3]
        NP1c = beta/inputParams[7]
        # Fitness constraint = Injected Dose = NP0*E*alpha = (beta*48*3600)*NP0t/NPc
        max_dose = 55

        constraint1 = inputParams[2]*inputParams[3]*alpha 
        constraint1 = constraint1/max_dose

        constraint2 = inputParams[6]*inputParams[7]*alpha 
        constraint2 = constraint2/max_dose

        constraint = constraint1 + constraint2
        # If ID is too high then don't need to evaluate
        if (inputParams[2]*inputParams[3]*alpha > max_dose) | (inputParams[6]*inputParams[7]*alpha > max_dose):
            data =  (0, 0, 0)

        print(f'ID for NP0 = {inputParams[2]*inputParams[3]*alpha:3.5f} mg/kg')
        print(f'ID for NP1 = {inputParams[6]*inputParams[7]*alpha:3.5f} mg/kg')
        VCParam = {'P0':{"k_a" :0, "D" :inputParams[0],"NP0" :NP0t}
                     ,'P1':{"k_a" :0, "D" :inputParams[4],"NP0" :NP1t}}
        CCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0], "NP_max" : NP0c}
                     ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4], "NP_max" : 10000000}}
        CSCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0], "NP_max" : 10000000}
                     ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4], "NP_max" : NP1c}}
        HCParam = {'P0':{"k_a" : 0,"k_d" :1e-4,"D" :inputParams[0]}
                     ,'P1':{"k_a" :0,"k_d" :1e-4,"D" :inputParams[4]}}
        
        stepsParams = {"cell":{"VC": VCParam, "CC": CCParam, "HC": HCParam, "CSC": CSCParam}}
    else:
        print(f'total NPs should be {inputParams[2]}')

        NP0t = inputParams[2]
        alpha = 2.56e-8 # = Molar mass(543.2g/mol)*Tumour volume (5mm^3)/(Mouse weight(20g)*Ncells(22)*CellLength^3(10um*10um*10um)*Avogadro(6e23))
        beta = 6022140 # = Potency(10uM)*CellLength^2 (10um*10um)*Avogadro(6e23)
        NPc = beta/inputParams[3]
        # Fitness constraint = Injected Dose = NP0*E*alpha = (beta*48*3600)*NP0t/NPc
        max_dose = 55

        constraint = inputParams[2]*inputParams[3]*alpha 
        constraint = constraint/max_dose

        #newest
        VCParam = {'P0':{"k_a" :0, "D" :inputParams[0],"NP0" :NP0t}} 
                     # ,'P1':{"k_a" :0, "D" :inputParams[4],"NP0" :inputParams[6]/inputParams[7], "T" :inputParams[7]}}
        CCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0], "NP_max" : NPc}}
                     # ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4]}}
        # CSCParam = {'P0':{"k_a" :inputParams[1],"k_d" :1e-4,"D" :inputParams[0]} 
        #              ,'P1':{"k_a" :inputParams[5],"k_d" :1e-4,"D" :inputParams[4]}}
        HCParam = {'P0':{"k_a" : 0,"k_d" :1e-4,"D" :inputParams[0]}} 
                     # 'P1':{"k_a" :0,"k_d" :1e-4,"D" :inputParams[4]}}
        
        stepsParams = {"cell":{"VC": VCParam, "CC": CCParam, "HC": HCParam}}#, "CSC": CSCParam}}    
        
    sim_opt, cell_data =  load_settings(stepsParams)
    data = (sim_opt, cell_data, constraint)
    main(data)
