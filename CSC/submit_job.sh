#!/bin/bash

#PBS -l nodes=1:ppn=16
#PBS -l walltime=24:00:00

# on compute node, change directory to 'submission directory':
cd $PBS_O_WORKDIR

output = out4.txt
# run your program, timing it for good measure:
time python ./optimize.py  | tee -a $output;
