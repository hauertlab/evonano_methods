/*
###############################################################################
# If you use PhysiCell in your project, please cite PhysiCell and the version #
# number, such as below:                                                      #
#                                                                             #
# We implemented and solved the model using PhysiCell (Version x.y.z) [1].    #
#                                                                             #
# [1] A Ghaffarizadeh, R Heiland, SH Friedman, SM Mumenthaler, and P Macklin, #
#     PhysiCell: an Open Source Physics-Based Cell Simulator for Multicellu-  #
#     lar Systems, PLoS Comput. Biol. 14(2): e1005991, 2018                   #
#     DOI: 10.1371/journal.pcbi.1005991                                       #
#                                                                             #
# See VERSION.txt or call get_PhysiCell_version() to get the current version  #
#     x.y.z. Call display_citations() to get detailed information on all cite-#
#     able software used in your PhysiCell application.                       #
#                                                                             #
# Because PhysiCell extensively uses BioFVM, we suggest you also cite BioFVM  #
#     as below:                                                               #
#                                                                             #
# We implemented and solved the model using PhysiCell (Version x.y.z) [1],    #
# with BioFVM [2] to solve the transport equations.                           #
#                                                                             #
# [1] A Ghaffarizadeh, R Heiland, SH Friedman, SM Mumenthaler, and P Macklin, #
#     PhysiCell: an Open Source Physics-Based Cell Simulator for Multicellu-  #
#     lar Systems, PLoS Comput. Biol. 14(2): e1005991, 2018                   #
#     DOI: 10.1371/journal.pcbi.1005991                                       #
#                                                                             #
# [2] A Ghaffarizadeh, SH Friedman, and P Macklin, BioFVM: an efficient para- #
#     llelized diffusive transport solver for 3-D biological simulations,     #
#     Bioinformatics 32(8): 1256-8, 2016. DOI: 10.1093/bioinformatics/btv730  #
#                                                                             #
###############################################################################
#                                                                             #
# BSD 3-Clause License (see https://opensource.org/licenses/BSD-3-Clause)     #
#                                                                             #
# Copyright (c) 2015-2018, Paul Macklin and the PhysiCell Project             #
# All rights reserved.                                                        #
#                                                                             #
# Redistribution and use in source and binary forms, with or without          #
# modification, are permitted provided that the following conditions are met: #
#                                                                             #
# 1. Redistributions of source code must retain the above copyright notice,   #
# this list of conditions and the following disclaimer.                       #
#                                                                             #
# 2. Redistributions in binary form must reproduce the above copyright        #
# notice, this list of conditions and the following disclaimer in the         #
# documentation and/or other materials provided with the distribution.        #
#                                                                             #
# 3. Neither the name of the copyright holder nor the names of its            #
# contributors may be used to endorse or promote products derived from this   #
# software without specific prior written permission.                         #
#                                                                             #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  #
# POSSIBILITY OF SUCH DAMAGE.                                                 #
#                                                                             #
###############################################################################
*/

#include "./evo_nano_3D.h"
// declare cell definitions here

Cell_Definition cancerCell;
Cell_Definition cancerStemCell;
Cell_Definition vascularCell;

void create_cell_types( void )
{
	// use the same random seed so that future experiments have the
	// same initial histogram of oncoprotein, even if threading means
	// that future division and other events are still not identical
	// for all runs

	SeedRandom( parameters.ints("random_seed") ); // or specify a seed here

	// housekeeping

	initialize_default_cell_definition();
	cell_defaults.phenotype.secretion.sync_to_microenvironment( &microenvironment );

	// Name the default cell type

	cell_defaults.type = 0;
	cell_defaults.name = "tumor cell";

	// set default cell cycle model
    live.transition_rate(0,0) = 0.0432; // default rate is 0.0432 / 60.0
	cell_defaults.functions.cycle_model = live;

	// set default_cell_functions;

	cell_defaults.functions.update_phenotype = update_cell_and_death_parameters_O2_based;

	// only needed for a 2-D simulation:

	/*
	cell_defaults.functions.set_orientation = up_orientation;
	cell_defaults.phenotype.geometry.polarity = 1.0;
	cell_defaults.phenotype.motility.restrict_to_2D = true;
	*/

	// make sure the defaults are self-consistent.

	cell_defaults.phenotype.secretion.sync_to_microenvironment( &microenvironment );
	cell_defaults.phenotype.sync_to_functions( cell_defaults.functions );

	// set the rate terms in the default phenotype

	// first find index for a few key variables.
	int apoptosis_model_index = cell_defaults.phenotype.death.find_death_model_index( "Apoptosis" );
	int necrosis_model_index = cell_defaults.phenotype.death.find_death_model_index( "Necrosis" );
	int oxygen_substrate_index = microenvironment.find_density_index( "oxygen" );

	int G0G1_index = Ki67_advanced.find_phase_index( PhysiCell_constants::G0G1_phase );
	int S_index = Ki67_advanced.find_phase_index( PhysiCell_constants::S_phase );

	// initially no necrosis
	cell_defaults.phenotype.death.rates[necrosis_model_index] = 0.0;

	// set oxygen uptake / secretion parameters for the default cell type
	cell_defaults.phenotype.secretion.uptake_rates[oxygen_substrate_index] = 10;
	cell_defaults.phenotype.secretion.secretion_rates[oxygen_substrate_index] = 0;
	cell_defaults.phenotype.secretion.saturation_densities[oxygen_substrate_index] = 38;

	// Create All specific cell definitions here

    createCellDefinitions();


	return;
}

void printParameters(Cell_Definition* cellDefinition){
    std::cout << cellDefinition->type << std::endl;
    std::cout << cellDefinition->name << std::endl;
}

void createCellDefinitions(){


    CellDefinitionBuilder *builder = new CancerStemCellDefinitionBuilder();
    cancerStemCell = *(builder->configureCellDefinition()->build());

    //printParameters(&cancerStemCell);

    builder = new CancerCellDefinitionBuilder();
    cancerCell = *(builder->configureCellDefinition()->build());

    //printParameters(&cancerCell);

    builder = new VascularityCellDefinitionBuilder();
    vascularCell = *(builder->configureCellDefinition()->build());

    //printParameters(&vascularCell);

}

void setup_microenvironment( void )
{
	// set domain parameters

/*
	default_microenvironment_options.X_range = {-500, 500};
	default_microenvironment_options.Y_range = {-500, 500};
	default_microenvironment_options.Z_range = {-500, 500};
*/
	// make sure to override and go back to 2D
	if( default_microenvironment_options.simulate_2D == true )
	{
		std::cout << "Warning: overriding XML config option and setting to 3D!" << std::endl;
		default_microenvironment_options.simulate_2D = false;
	}


	// no gradients need for this example

	default_microenvironment_options.calculate_gradients = false;

	// set Dirichlet conditions

	default_microenvironment_options.outer_Dirichlet_conditions = true;

	// if there are more substrates, resize accordingly
	std::vector<double> bc_vector( 1 , 38.0 ); // 5% o2
	default_microenvironment_options.Dirichlet_condition_vector = bc_vector;

	// initialize BioFVM

	initialize_microenvironment();

	return;
}

void setup_tissue( void )
{
	// create some cells near the origin
    CellFactory *cellFactory = new CellFactory();

    Cell* pc;

    pc = cellFactory->createCell(nullptr, &cancerCell);
    pc->assign_position( 0.0, 0.0, 0.0 );

    create_capillary(cellFactory);

	return;
}

void create_capillary(CellFactory* cellFactory)
{

    int load_capillary = parameters.ints("load_capillary");
    std::string fileName = parameters.strings("capillary_file_name");

    std::cout << (load_capillary == 0 ? "Creating capillary " : "Loading capillary") << " (" <<  fileName << ")" << endl << endl;
    double cell_radius = 10;
    double dt = 0.01;

    if (load_capillary == 0) {
        create_new_capillary(cellFactory, cell_radius, dt, fileName);
    } else {
        load_saved_capillary(cellFactory, cell_radius, dt, fileName);
    }

}

void create_new_capillary(CellFactory* cellFactory, double cell_radius, double dt, std::string fileName)
{

    int start_x = parameters.ints("vascularity_start_position_x");
    int start_y = parameters.ints("vascularity_start_position_y");
    int start_z = parameters.ints("vascularity_start_position_z");
    int vascularity_cell_amount = parameters.ints("vascularity_cell_amount");
    int vascularity_number_of_capillary = parameters.ints("vascularity_number_of_capillary");

    int capillary_length = vascularity_cell_amount / vascularity_number_of_capillary;

    //get boundaries of microenvironment
    int min_x = default_microenvironment_options.X_range[0];
    int max_x = default_microenvironment_options.X_range[1];
    int min_y = default_microenvironment_options.Y_range[0];
    int max_y = default_microenvironment_options.Y_range[1];
    int min_z = default_microenvironment_options.Z_range[0];
    int max_z = default_microenvironment_options.Z_range[1];


    std::vector<Capillary*> capillaries;

    Point starting_position(start_x, start_y, start_z);
    Capillary *firstCapillary = new Capillary(starting_position, capillary_length, 10,
                                              10); //position, length, straightness, step distance
    capillaries.push_back(firstCapillary); //add first capillary

    int depth = 0; // depth of the origin capillary
    int capillary_on_depth = 2; //number of capillary on current depth

    for (int i = 0; i < vascularity_number_of_capillary - 1; i++) {

        if (capillary_on_depth == 0) {
            depth++;
            capillary_on_depth = 2 << depth;
        }

        int index = get_capillary_index_by_depth(depth); // get random capilary on given depth
        Point point = capillaries[index]->getEndPosition(); //get starting point for new capillary

        Capillary *c = new Capillary(point, capillary_length, 10, 10);
        capillaries.push_back(c);

        capillary_on_depth--;
    }



    for (int i = 0; i < capillaries.size(); i++) {
        for (int j = 0; j < capillaries[i]->getLength(); j += 2) {
            Capillary *c = capillaries[i];

            if ((*c)[j].getX() < max_x - cell_radius * 2 && (*c)[j].getX() > min_x + cell_radius * 2 && //check if cell is inbound
                (*c)[j].getY() < max_y - cell_radius * 2 && (*c)[j].getY() > min_y + cell_radius * 2 &&
                (*c)[j].getZ() < max_z - cell_radius * 2 && (*c)[j].getZ() > min_z + cell_radius * 2) {
                create_capillary_cell(cellFactory, (*c)[j].getX(), (*c)[j].getY(), (*c)[j].getZ(), cell_radius, // make vascularity cells
                                      dt);
            }
        }
    }

    save_capillary(capillaries, fileName);

}


void save_capillary(std::vector<Capillary*> capillaries, std::string fileName){

    ofstream file;
    file.open (fileName);

    file << capillaries.size() << " " << capillaries[0]->getLength() << endl;
    for(int i = 0; i < capillaries.size(); i++){
        //cout << *(capillaries[i]);
        file << *(capillaries[i]);
    }

    file.close();
}
void load_saved_capillary(CellFactory* cellFactory, double cell_radius, double dt, std::string fileName)
{
    ifstream file;
    file.open(fileName);

    int num_of_capillaries, length_of_capillary;
    file >> num_of_capillaries >> length_of_capillary;
    std::string line;
    int x,y,z;

    std::vector<Capillary*> capillaries;
    while (std::getline(file, line))
    {

        if (line.empty()){
            continue;
        }
        int i = 0;
        Capillary* c = new Capillary(Point(0,0,0),length_of_capillary, 10, 10);

        std::istringstream iss(line);

        while(iss >> x >> y >> z) {
            c->setPoint(i, x, y, z);
            i++;

        }
        capillaries.push_back(c);
    }

    file.close();

    for (int i = 0; i < capillaries.size(); i++) {
        for (int j = 0; j < capillaries[i]->getLength(); j += 2) {
            Capillary *c = capillaries[i];
            create_capillary_cell(cellFactory, (*c)[j].getX(), (*c)[j].getY(), (*c)[j].getZ(), cell_radius, dt); // make vascularity cell
        }
    }

}

void create_capillary_cell(CellFactory* cellFactory, int x, int y, int z, double cell_radius, double dt)
{
    Cell* vascular_cell = cellFactory->createCell(nullptr, &vascularCell);
    vascular_cell->assign_position(x, y, z);
    vascular_cell->set_total_volume((4.0/3.0)*3.14*pow(cell_radius,3.0) );
    //already set for vascularCell definition, change here if in need of using different parameters for different cells
    (*vascular_cell->secretion_rates)[0]=10;
    (*vascular_cell->saturation_densities)[0]=70;
    vascular_cell->set_internal_uptake_constants(dt);
}

std::vector<std::string> my_coloring_function( Cell* pCell )
{
	// start with flow cytometry coloring
	std::vector<std::string> output = false_cell_coloring_cytometry(pCell);

	// if the cell is motile and not dead, paint it black

    if( pCell->phenotype.death.dead == false &&
        pCell->type == 1 )
    {
        output[0] = "limegreen";
        output[2] = "limegreen";
    }
    if( pCell->phenotype.death.dead == false &&
        pCell->type == 2 )
    {
        output[0] = "mediumaquamarine";
        output[2] = "mediumaquamarine";
    }
    if(pCell->phenotype.death.dead == false && pCell->type == 3){
        output[0] = "red";
        output[2] = "red";
    }

	return output;
}
