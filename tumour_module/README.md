# EVO-NANO CANCER SIMULATOR

##### **Version:** 1.0.0 **Release date:** TBA

#####   [**EVO-NANO**](http://evonano.eu/) is **Horizon2020** project supported by the **EU**.  The purpose of this simulator is to imitate cancer growth and its treatment using different medications. Simulator is heavily based on [PhysiCell](https://github.com/MathCancer/PhysiCell) open source framework( **Version:** 1.4.1 **Release date:** 2 October 2018).
