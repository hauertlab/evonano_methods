//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#include "CellDefinitionBuilder.h"
#include "../../modules/PhysiCell_standard_modules.h"

CellDefinitionBuilder *CancerStemCellDefinitionBuilder::configureCellDefinition() {
    // first find index for a few key variables.
    int apoptosis_model_index = cell_defaults.phenotype.death.find_death_model_index( "Apoptosis" );
    int necrosis_model_index = cell_defaults.phenotype.death.find_death_model_index( "Necrosis" );
    int oxygen_substrate_index = microenvironment.find_density_index( "oxygen" );

    int G0G1_index = Ki67_advanced.find_phase_index( PhysiCell_constants::G0G1_phase );
    int S_index = Ki67_advanced.find_phase_index( PhysiCell_constants::S_phase );

    cellDefinition = new PhysiCell::Cell_Definition(cell_defaults); // TODO 2 : COPY CELL_DEFAULTS
    cellDefinition->type = 2;
    cellDefinition->name = "CSC";

    // make sure the new cell type has its own reference phenotyhpe

    cellDefinition->parameters.pReference_live_phenotype = &( cellDefinition->phenotype );
    double o2_necrosis_threshold; // value at which cells start experiencing necrotic death
    double o2_necrosis_max; // value at which necrosis reaches its maximum rate
    cellDefinition->parameters.o2_necrosis_threshold = parameters.doubles( "cancer_stem_cell_o2_necrosis_threshold" );
    cellDefinition->parameters.o2_necrosis_max = parameters.doubles( "cancer_stem_cell_o2_necrosis_max" );
    // enable random motility
    //cancer_stem_cell->phenotype.motility.is_motile = true;
    cellDefinition->phenotype.motility.persistence_time = parameters.doubles( "cancer_stem_cell_persistence_time" ); // 15.0; // 15 minutes
    cellDefinition->phenotype.motility.migration_speed = parameters.doubles( "cancer_stem_cell_migration_speed" ); // 0.25; // 0.25 micron/minute
    //cancer_stem_cell->phenotype.motility.migration_bias = 0.0;// completely random


    // Set cell-cell adhesion to 5% of other cells
    cellDefinition->phenotype.mechanics.cell_cell_adhesion_strength *=
            parameters.doubles( "cancer_stem_cell_relative_adhesion" ); // 0.05;

    // Set apoptosis to zero
    cellDefinition->phenotype.death.rates[apoptosis_model_index] =
            parameters.doubles( "cancer_stem_cell_apoptosis_rate" ); // 0.0;

    // Set proliferation to 10% of other cells.
    // Alter the transition rate from G0G1 state to S state
    cellDefinition->phenotype.cycle.data.transition_rate(G0G1_index,S_index) *=
            parameters.doubles( "cancer_stem_cell_relative_cycle_entry_rate" ); // 0.1;

    return this;
}
