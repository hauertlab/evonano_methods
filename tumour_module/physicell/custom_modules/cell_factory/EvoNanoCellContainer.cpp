//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#include "EvoNanoCellContainer.h"
void EvoNanoCellContainer::updateAllCells(double t) {
    // secretions and uptakes. Syncing with BioFVM is automated.

#pragma omp parallel for
    for( int i=0; i < (*all_cells).size(); i++ )
    {
        (*all_cells)[i]->phenotype.secretion.advance( (*all_cells)[i], (*all_cells)[i]->phenotype , diffusion_dt );
    }

    //if it is the time for running cell cycle, do it!
    double time_since_last_cycle= t- last_cell_cycle_time;

    static double phenotype_dttolerance = 0.001 * phenotype_dt;
    static double mechanics_dttolerance = 0.001 * mechanics_dt;

    if( fabs(time_since_last_cycle-phenotype_dt ) < phenotype_dttolerance || !initialzed)
    {
        // Reset the max_radius in each voxel. It will be filled in set_total_volume
        // It might be better if we calculate it before mechanics each time
        std::fill(max_cell_interactive_distance_in_voxel.begin(), max_cell_interactive_distance_in_voxel.end(), 0.0);

        if(!initialzed)
        {
            time_since_last_cycle = phenotype_dt;
        }

        // new as of 1.2.1 -- bundles cell phenotype parameter update, volume update, geometry update,
        // checking for death, and advancing the cell cycle. Not motility, though. (that's in mechanics)
#pragma omp parallel for
        for( int i=0; i < (*all_cells).size(); i++ )
        {
            if((*all_cells)[i]->is_out_of_domain)
            { continue; }
            // (*all_cells)[i]->phenotype.advance_bundled_models( (*all_cells)[i] , time_since_last_cycle );
            (*all_cells)[i]->advance_bundled_phenotype_functions( time_since_last_cycle );
        }

        // process divides / removes
        if(cells_ready_to_divide.size() > 0) {
            cellDivision(&cells_ready_to_divide);
        }

        for( int i=0; i < cells_ready_to_die.size(); i++ )
        {
            cells_ready_to_die[i]->die();
            (*mapNumberOfDeadCellsByType)[cells_ready_to_die[i]->type_name]++;
        }
        num_divisions_in_current_step+=  cells_ready_to_divide.size();
        num_deaths_in_current_step+=  cells_ready_to_die.size();

        cells_ready_to_die.clear();
        cells_ready_to_divide.clear();
        last_cell_cycle_time= t;
    }

    double time_since_last_mechanics= t- last_mechanics_time;

    // if( time_since_last_mechanics>= mechanics_dt || !initialzed)
    if( fabs(time_since_last_mechanics - mechanics_dt) < mechanics_dttolerance || !initialzed)
    {
        if(!initialzed)
        {
            time_since_last_mechanics = mechanics_dt;
        }

        // new February 2018
        // if we need gradients, compute them
        if( default_microenvironment_options.calculate_gradients )
        { microenvironment.compute_all_gradient_vectors();  }
        // end of new in Feb 2018

        // Compute velocities
#pragma omp parallel for
        for( int i=0; i < (*all_cells).size(); i++ )
        {

            if(!(*all_cells)[i]->is_out_of_domain && (*all_cells)[i]->is_movable && (*all_cells)[i]->functions.update_velocity )
            {
                // update_velocity already includes the motility update
                //(*all_cells)[i]->phenotype.motility.update_motility_vector( (*all_cells)[i] ,(*all_cells)[i]->phenotype , time_since_last_mechanics );
                (*all_cells)[i]->functions.update_velocity( (*all_cells)[i], (*all_cells)[i]->phenotype, time_since_last_mechanics);
            }

            if( (*all_cells)[i]->functions.custom_cell_rule )
            {
                (*all_cells)[i]->functions.custom_cell_rule((*all_cells)[i], (*all_cells)[i]->phenotype, time_since_last_mechanics);
            }
        }
        // Calculate new positions
#pragma omp parallel for
        for( int i=0; i < (*all_cells).size(); i++ )
        {
            if(!(*all_cells)[i]->is_out_of_domain && (*all_cells)[i]->is_movable)
            {
                (*all_cells)[i]->update_position(time_since_last_mechanics);
            }
        }

        // When somebody reviews this code, let's add proper braces for clarity!!!

        // Update cell indices in the container
        for( int i=0; i < (*all_cells).size(); i++ )
            if(!(*all_cells)[i]->is_out_of_domain && (*all_cells)[i]->is_movable)
                (*all_cells)[i]->update_voxel_in_container();
        last_mechanics_time=t;
    }

    initialzed=true;
    return;
}

Cell* EvoNanoCellContainer::cellDivision(std::vector<Cell *> *cells_ready_to_divide, std::vector<bool> probabilities) {
    Cell *lastChildCell;
    for( int i=0; i < cells_ready_to_divide->size(); i++ )
    {
        EvoNanoCell *cell = (EvoNanoCell*)((*cells_ready_to_divide)[i]);
        probabilities.clear();
        if(probabilities.size() <= 0){

            if (cell->type_name == "CC") {
                bool shouldDevelopCSC = Util::calculate_probability_for_developing_new_cell(0, 100, 100, 0.995); // false is CC, true is CSC
                probabilities.push_back(shouldDevelopCSC); // 99.5% CC, 0.5% CSC
                probabilities.push_back(false); //CC
            }else if( cell->type_name == "CSC") {
                bool shouldDivideSymmetric = Util::calculate_probability_for_developing_new_cell(0, 100, 100, 0.01);
                if(shouldDivideSymmetric == true){
                    probabilities.push_back(true); // CSC
                    probabilities.push_back(false); // CC
                } else {
                    bool shouldDivideToTwoCSC = Util::calculate_probability_for_developing_new_cell(0, 100, 100, 0.01);
                    if(shouldDivideToTwoCSC == true) {
                        probabilities.push_back(false); // CC
                        probabilities.push_back(false); // CC
                    } else {
                        probabilities.push_back(true); // CSC
                        probabilities.push_back(true); // CSC
                    }
                }
            }
        }

        std::map<Cell_Definition *, bool> *definitionMap = new std::map<Cell_Definition *, bool>();
        CellDefinitionBuilder *cellDefinitionBuilder;
        Cell_Definition *cellDefinition;
        if (cell->type_name == "CC") {
            cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
        }else if( cell->type_name == "CSC") {
            cellDefinitionBuilder = new CancerCellDefinitionBuilder();
        }
        cellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
        (*definitionMap)[new Cell_Definition(*cellDefinition)] = probabilities[0];
        (*definitionMap)[new Cell_Definition(*cellDefinition)] = probabilities[1];
        std::vector<Cell_Definition*> *returnedDefinitions = createCellDefinitions(definitionMap);
        Cell_Definition *cloneTemplateCellDefinition = nullptr;
        Cell_Definition *changeTypeTemplateCellDefinition = nullptr;


        if((*returnedDefinitions)[0] != nullptr) {
            cloneTemplateCellDefinition = new Cell_Definition(*((*returnedDefinitions)[0]));
        }
        if((*returnedDefinitions)[1] != nullptr){
            changeTypeTemplateCellDefinition = new Cell_Definition(*((*returnedDefinitions)[1]));
        }

        lastChildCell = cell->divide(cloneTemplateCellDefinition, changeTypeTemplateCellDefinition);
    }

    return lastChildCell;
}

std::vector<Cell_Definition *> * EvoNanoCellContainer::createCellDefinitions(std::map<Cell_Definition *, bool> *definitionMap) {

    std::vector<Cell_Definition *> *retVal = new std::vector<Cell_Definition *>();

    for( const auto& cd : *definitionMap) {
        if(cd.second == true) {
            retVal->push_back(cd.first);
        } else {
            retVal->push_back(nullptr);
        }
    }

    return retVal;
}

EvoNanoCellContainer* create_evo_nano_cell_container_for_microenvironment( BioFVM::Microenvironment& m , double mechanics_voxel_size )
{
    EvoNanoCellContainer* cell_container = new EvoNanoCellContainer;
    cell_container->initialize( m.mesh.bounding_box[0], m.mesh.bounding_box[3],
                                m.mesh.bounding_box[1], m.mesh.bounding_box[4],
                                m.mesh.bounding_box[2], m.mesh.bounding_box[5],  mechanics_voxel_size );
    m.agent_container = (Agent_Container*) cell_container;

    if( BioFVM::get_default_microenvironment() == NULL )
    {
        BioFVM::set_default_microenvironment( &m );
    }

    return cell_container;
}

std::unordered_map<std::string, int> * EvoNanoCellContainer::getMapNumberOfDeadCellsByType() {
    return mapNumberOfDeadCellsByType;
}

void EvoNanoCellContainer::setMapNumberOfDeadCellsByType(std::vector <std::string> keys, std::vector<int> values) {
    if(keys.size() != values.size()){
        return;
    }

    mapNumberOfDeadCellsByType->clear();

    for(int i = 0; i < keys.size(); i++){
        (*mapNumberOfDeadCellsByType)[keys[i]] = values[i];
    }
}