//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#include "CellDefinitionBuilder.h"
Cell_Definition *CellDefinitionBuilder::build() {
    return cellDefinition;
}

CellDefinitionBuilder::~CellDefinitionBuilder() {
    delete cellDefinition;
}