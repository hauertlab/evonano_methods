//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#ifndef EVO_NANO_EVONANOCELLCONTAINER_HPP
#define EVO_NANO_EVONANOCELLCONTAINER_HPP

#include "../../core/PhysiCell.h"
#include "../../core/PhysiCell_cell_container.h"
#include "EvoNanoCell.h"
#include "../helpers/Util.h"
#include <vector>
#include <map>
using namespace PhysiCell;

class EvoNanoCellContainer : public Cell_Container {
private:
    std::unordered_map<std::string, int> *mapNumberOfDeadCellsByType;
public:
    EvoNanoCellContainer():Cell_Container(){
        mapNumberOfDeadCellsByType = new std::unordered_map<std::string, int>();
    }
    void updateAllCells(double t);
    Cell* cellDivision(std::vector<Cell *> *cells_ready_to_divide, std::vector<bool> probabilities = {});
    std::vector<Cell_Definition *> *createCellDefinitions(std::map<Cell_Definition *, bool> *definitionMap);
    std::unordered_map<std::string, int> *getMapNumberOfDeadCellsByType();
    void setMapNumberOfDeadCellsByType(std::vector<std::string> keys, std::vector<int> values);
    ~EvoNanoCellContainer(){
        delete mapNumberOfDeadCellsByType;
        mapNumberOfDeadCellsByType = nullptr;
    }
};

EvoNanoCellContainer* create_evo_nano_cell_container_for_microenvironment( BioFVM::Microenvironment& m , double mechanics_voxel_size );

#endif //EVO_NANO_EVONANOCELLCONTAINER_HPP
