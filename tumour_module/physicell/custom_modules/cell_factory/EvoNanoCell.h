//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#ifndef EVO_NANO_CELL_FACTORY_H
#define EVO_NANO_CELL_FACTORY_H
#include "../../core/PhysiCell.h"
#include "CellDefinitionBuilder.h"
using namespace PhysiCell;


class EvoNanoCell : public Cell {
public:
    EvoNanoCell(Cell *cellToCloneFrom, Cell_Definition *templateCellDefinition);
    EvoNanoCell();
    ~EvoNanoCell();
    void deleteCell(Cell *cell);
    /**
     *
     * @param templateCellDefinition
     * @return deep copy of the current cell or of the cell provided as @param templateCellDefinition
     */
    Cell *clone(Cell *cellToCloneFrom, Cell_Definition *templateCellDefinition = nullptr);
    Cell *divide(Cell_Definition *cloneTemplateCellDefinition = nullptr,
                 Cell_Definition *changeTypeTemplateCellDefinition = nullptr);
    void changeCellType(Cell *cell, Cell_Definition *templateCellDefinition);

};

class CellFactory {
public:
    EvoNanoCell* createCell(Cell *templateCell= nullptr, Cell_Definition *templateCellDefinition= nullptr);
};


/*
Example of factory and builder usage
CellFactory *cellFactory = new CellFactory();
CellDefinitionBuilder *builder = new CancerStemCellDefinitionBuilder();
CancerStemCellDefinition *cancerStemCellDefinition = builder->configureCellDefinition()->build();
builder = new CancerCellDefinitionBuilder();
CancerCellDefinition *cancerStemCellDefinition = builder->configureCellDefinition()->build();

EvoNanoCell* cancerCell = cellFactory->createCell(cancerStemCellDefinition);
EvoNanoCell* cancerStemCell = cellFactory->createCell(cancerCell);
cancerCell->clone(cancerStemCellDefinition);
*/


#endif //EVO_NANO_CELL_FACTORY_H
