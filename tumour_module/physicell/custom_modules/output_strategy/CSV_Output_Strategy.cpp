//
// Created by Mihailo Vasiljevic on 2019-05-06.
//

#include "Output_Strategy.h"

void CSV_Output_Strategy::write_output(std::string filename, double time, std::vector<PhysiCell::Cell *> *all_cells,
        PhysiCell::PhysiCell_Settings &physicell_settings,Microenvironment_Options &default_microenvironment_options,
                                       EvoNanoCellContainer *container ) {
    std::ofstream output_file;
    output_file.open (filename, std::fstream::in | std::fstream::out | std::fstream::app);

    if(header_already_written == false){
        write_header(output_file, all_cells, physicell_settings, default_microenvironment_options);
    }

    if(lastNumberOfCells != all_cells->size() || true){
        lastNumberOfCells = all_cells->size();

        int countCancerCells = 0;
        int count_cancer_stem_cells = 0;
        for(int i = 0; i < all_cells->size(); i++){
            int cell_type = (*all_cells)[i]->type;
            if( cell_type == 1){
                countCancerCells++;
            } else if(cell_type == 2) {
                count_cancer_stem_cells++;
            }
        }
        output_file << " \n";
        output_file << "Simulation:\n";
        output_file << "Time: " << time << "\n";
        output_file << "Total amount of cells: " << all_cells->size() << "\n";
        output_file << "Number of CSC: " << count_cancer_stem_cells << "\n";
        output_file << "Number of CC: " << countCancerCells << "\n";
        output_file << "Number of dead CSC: " << (*container->getMapNumberOfDeadCellsByType())["CSC"] << "\n";
        output_file << "Number of dead CC: " << (*container->getMapNumberOfDeadCellsByType())["CC"] << "\n";
        output_file << "Cell, type, volume, cycle model, cycle phase, x, y, z\n";
        std::string row = "";
        int row_counter = 0;
        for(int i = 0; i < all_cells->size(); i++) {
            int cell_type = (*all_cells)[i]->type;
            if (cell_type == 1 || cell_type == 2 || cell_type == 3) {
                row_counter++;
                row = "";
                row += std::to_string(row_counter);
                row += ",";
                row += (*all_cells)[i]->type_name;
                row += ",";
                row += std::to_string((*all_cells)[i]->phenotype.volume.total);
                row += ",";
                row += (*all_cells)[i]->phenotype.cycle.pCycle_Model->name;
                row += ",";
                row += (*all_cells)[i]->phenotype.cycle.current_phase().name;
                row += ",";
                for (int j = 0; j < (*all_cells)[i]->position.size(); j++) {
                    row += std::to_string((*all_cells)[i]->position[j]);
                    if (j <= (*all_cells)[i]->position.size() - 2) {
                        row += ",";
                    }
                }

                row += "\n";
                // std::cout << row << std::endl;
                output_file << row << "\n";
            }
        }

    }


    output_file.close();
}

void CSV_Output_Strategy::write_header(std::ofstream &output_file, std::vector<PhysiCell::Cell *> *all_cells, PhysiCell::PhysiCell_Settings &physicell_settings, Microenvironment_Options &default_microenvironment_options) {
    header_already_written = true;

    output_file << " \n";
    output_file << "Simulation parameters:\n";
    output_file << "Space: , " << (default_microenvironment_options.simulate_2D ? "2D" : "3D") << "\n";
    output_file << "x: ," << default_microenvironment_options.X_range[1] << "\n";
    output_file << "y: ," << default_microenvironment_options.Y_range[1] << "\n";
    output_file << "z: ," << default_microenvironment_options.Z_range[1] << "\n";
    output_file << "Simulation length: ," << physicell_settings.max_time << "\n";
    output_file << "Starting number of cells: ," << all_cells->size() << "\n";
}