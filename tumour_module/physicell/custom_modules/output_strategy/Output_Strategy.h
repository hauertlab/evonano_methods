#ifndef EVO_NANO_OUTPUT_STRATEGY_H
#define EVO_NANO_CELL_FACTORY_H
#include <memory>
#include <string>
#include "../../core/PhysiCell.h"
#include <vector>
#include <iostream>
#include <fstream>
#include "../../modules/PhysiCell_settings.h"
#include "../cell_factory/EvoNanoCellContainer.h"
class Output_Strategy{
    public:
        virtual void write_output(std::string filename, double time, std::vector<PhysiCell::Cell*> *all_cells,
                PhysiCell::PhysiCell_Settings &physicell_settings, Microenvironment_Options &default_microenvironment_options,
                EvoNanoCellContainer *container)=0;
};

class CSV_Output_Strategy : public Output_Strategy {
private:
    int lastNumberOfCells = 0;
    bool header_already_written = false;
    void write_header(std::ofstream &out_file, std::vector<PhysiCell::Cell *> *all_cells, PhysiCell::PhysiCell_Settings &physicell_settings, Microenvironment_Options &default_microenvironment_options);
public:
    void write_output(std::string filename, double time, std::vector<PhysiCell::Cell*> *all_cells,
            PhysiCell::PhysiCell_Settings &physicell_settings, Microenvironment_Options &default_microenvironment_options,
                      EvoNanoCellContainer *container);
};
#endif //EVO_NANO_CELL_FACTORY_H