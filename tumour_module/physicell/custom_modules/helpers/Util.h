//
// Created by Mihailo Vasiljevic on 2019-05-28.
//

#ifndef EVO_NANO_UTIL_HPP
#define EVO_NANO_UTIL_HPP


class Util {
public:
    static bool calculate_probability_for_developing_new_cell(int lower_boundary, int upper_boundary, int percentage_factor, double probability);
};


#endif //EVO_NANO_UTIL_HPP
