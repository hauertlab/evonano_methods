#include "Capillary.h"


//Point methods
bool Point::operator==(const Point& p){
    return x == p.x && y == p.y && z == p.z;
}

Point& Point::operator=(const Point& p){
    x = p.x;
    y = p.y;
    z = p.z;
    return *this;
}

vector<double> Point::getPointAsVector(){

    vector<double> points;

    points.push_back(x);
    points.push_back(y);
    points.push_back(z);

    return points;
}


ostream& operator<<(ostream& os, const Point& p){
    os << p.x << " " << p.y << " " << p.z << " ";
    return os;
}

istream& operator>>(istream& is, Point& p){

    is >> p.x >> p.y >> p.z;
    return is;
}

//Capillary methods
Capillary::Capillary(const Point& start, int stepps, int straightness, int stepDistance) {

    path = new Point[stepps];

    path[0] = start;

    length = 0;

    int i = 0;
    int x = path[0].getX();
    int y = path[0].getY();
    int z = path[0].getZ();

    while (i < stepps){
        int direction = rand() % 6;
        int numberOfStepps = rand() % straightness + 1;
        for (int j = 0; j < numberOfStepps; j++){
            switch(direction){
                case 0: x-=stepDistance; break;
                case 1: x+=stepDistance; break;
                case 2: y-=stepDistance; break;
                case 3: y+=stepDistance; break;
                case 4: z-=stepDistance; break;
                case 5: z+=stepDistance; break;
            }
            if (i < stepps){
                setPoint(i, x, y, z);
                length++;
                i++;
            }
        }
    }
}


void Capillary::setPoint(int i, int x, int y, int z){
    path[i] = Point(x,y,z);
}

int Capillary::getLength() const {
    return length;
}

Point Capillary::getEndPosition() const {
    return path[length - 1];
};

double Capillary::distance(vector<double>& position){

    double minDistance = 99999; // TODO: change minDistance to distance of first element
    for (int i = 0; i < length; i++){
        Point p = path[i];

        double dx = abs(p.getX() - position[0]);
        double dy = abs(p.getY() - position[1]);
        double dz = abs(p.getZ() - position[2]);

        double distance = sqrt(dx * dx + dy * dy + dz * dz);
        if (distance < minDistance){
            minDistance = distance;
        }
    }
    return minDistance;
}

Point Capillary::operator[](int i) const {
    return path[i];
}

Point& Capillary::operator[](int i) {
    return path[i];
}

ostream& operator<<(ostream& os, const Capillary& c){
    for (int i = 0; i < c.length; i++) {
        os << c.path[i];
    }
    os << endl;
    return os;
}

int get_capillary_index_by_depth(int depth) {
    if (depth < 2){
        return 0;
    }

    int lower = 1 << (depth - 2);
    int higher = 1 << (depth - 1);

    return rand() % (higher - lower) + lower;
}

/* Function for determining which cells are close to capillary
void checkCapillary(Capillary& c, int maxDistance){

    for (int i = 0; i < all_cells->size(); i++){
        vector<double> position = (*all_cells)[i]->position;
        if (c.distance(position) < maxDistance){
            cout << position[0] << " " << position[1] << " " << position[2] << endl;
        }
    }
}
 */