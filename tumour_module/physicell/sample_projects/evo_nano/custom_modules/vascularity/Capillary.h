#ifndef EVO_NANO_MASTER_CAPILLARY_H
#define EVO_NANO_MASTER_CAPILLARY_H

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Point {
private:
    int x;
    int y;
    int z;

public:
    Point(int x = 0, int y = 0, int z = 0) : x(x), y(y), z(z) { }

    int getX() const { return x;}
    int getY() const { return y;}
    int getZ() const { return z;}

    vector<double> getPointAsVector();

    bool operator==(const Point& p);
    Point& operator=(const Point& p);

    friend ostream& operator<< (ostream& os, const Point& p);
    friend istream& operator>> (istream& is, Point& p);

};

class Capillary {
private:
    Point* path;
    int length;
public:
    Capillary(const Point& start = Point(0,0,0), int stepps = 100, int straightness  = 10, int stepDistance = 1);
    ~Capillary(){ delete[] path;}

    int getLength() const;
    Point getEndPosition() const;

    void setPoint(int i, int x, int y, int z);
    double distance(vector<double>& position);

    Point operator[](int i) const;
    Point& operator[](int i);

    friend ostream& operator<<(ostream& os, const Capillary& c);
    friend istream& operator>>(istream& is, Capillary& c);

};

int get_capillary_index_by_depth(int); //returns index of a capillary at a given depth, used for generating vascularity network


#endif //EVO_NANO_MASTER_CAPILLARY_H
