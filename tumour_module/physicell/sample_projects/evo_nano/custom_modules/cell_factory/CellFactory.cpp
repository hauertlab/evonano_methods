//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#include "EvoNanoCell.h"

EvoNanoCell *CellFactory::createCell(Cell *templateCell,Cell_Definition *templateCellDefinition) {
    return new EvoNanoCell(templateCell, templateCellDefinition);
}