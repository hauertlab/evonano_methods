//
// Created by totma on 06-Jul-19.
//

#include "CellDefinitionBuilder.h"
#include "../../modules/PhysiCell_standard_modules.h"

CellDefinitionBuilder *VascularityCellDefinitionBuilder::configureCellDefinition() {


    cellDefinition = new PhysiCell::Cell_Definition(cell_defaults); // TODO 2 : COPY CELL_DEFAULTS
    cellDefinition->type = 3;
    cellDefinition->name = "VC";

    // make sure the new cell type has its own reference phenotyhpe
    cellDefinition->parameters.pReference_live_phenotype = &( cellDefinition->phenotype );

    // disable motility
    cellDefinition->phenotype.motility.is_motile = false; // vascular cells don't move
    // turn the default cycle model to live,
    // so it's easier to turn off proliferation

    cellDefinition->phenotype.cycle.sync_to_cycle_model( live );
    // turn off proliferation and death (copied from biorobors.cpp)
    int cycle_start_index = live.find_phase_index( PhysiCell_constants::live );
    int cycle_end_index = live.find_phase_index( PhysiCell_constants::live );

    int apoptosis_index = cell_defaults.phenotype.death.find_death_model_index( PhysiCell_constants::apoptosis_death_model );

    cellDefinition->phenotype.cycle.data.transition_rate( cycle_start_index , cycle_end_index ) = 0.0;
    cellDefinition->phenotype.death.rates[apoptosis_index] = 0.0;

    // set uptake and secretion to zero
    cellDefinition->phenotype.secretion.secretion_rates[0] = 10;
    cellDefinition->phenotype.secretion.uptake_rates[0] = 0;
    cellDefinition->phenotype.secretion.saturation_densities[0] = 70;

    cellDefinition->phenotype.secretion.secretion_rates[1] = 0;
    cellDefinition->phenotype.secretion.uptake_rates[1] = 0;
    cellDefinition->phenotype.secretion.saturation_densities[1] = 1;

    return this;
}
