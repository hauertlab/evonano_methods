//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#include "EvoNanoCell.h"

Cell *EvoNanoCell::clone(Cell *cellToCloneFrom, Cell_Definition *templateCellDefinition) {
    /**
     * If @param templateCellDefinition is provided then try to develop new cell using provided definition
     * Else use the parameters defined by the current cell that is dividing
     */
    CellFactory *cellFactory = new CellFactory();
    Cell *clonedCell = cellFactory->createCell(cellToCloneFrom, templateCellDefinition);


    return clonedCell;
}
Cell *EvoNanoCell::divide(Cell_Definition *cloneTemplateCellDefinition,
                          Cell_Definition *changeTypeTemplateCellDefinition) {

    changeCellType(this, changeTypeTemplateCellDefinition);

    Cell *child = clone(this, cloneTemplateCellDefinition);

    double temp_angle = 6.28318530717959*UniformRandom();
    double temp_phi = 3.1415926535897932384626433832795*UniformRandom();

    double radius= phenotype.geometry.radius;
    std::vector<double> rand_vec (3, 0.0);

    rand_vec[0]= cos( temp_angle ) * sin( temp_phi );
    rand_vec[1]= sin( temp_angle ) * sin( temp_phi );
    rand_vec[2]= cos( temp_phi );
    rand_vec = rand_vec- phenotype.geometry.polarity*(rand_vec[0]*state.orientation[0]+
                                                      rand_vec[1]*state.orientation[1]+rand_vec[2]*state.orientation[2])*state.orientation;


    if( norm(rand_vec) < 1e-16 )
    {
        std::cout<<"************ERROR********************"<<std::endl;
    }
    normalize( &rand_vec );
    // rand_vec/= norm(rand_vec);
    child->assign_position(position[0] + 0.5 * radius*rand_vec[0],
                           position[1] + 0.5 * radius*rand_vec[1],
                           position[2] + 0.5 * radius*rand_vec[2]);
    //change my position to keep the center of mass intact and then see if I need to update my voxel index
    static double negative_one_half = -0.5;
    naxpy( &position, negative_one_half , rand_vec );// position = position - 0.5*rand_vec;
    // position[0] -= 0.5*radius*rand_vec[0];
    // position[1] -= 0.5*radius*rand_vec[1];
    // position[2] -= 0.5*radius*rand_vec[2];

    update_voxel_in_container();
    phenotype.volume.divide();
    child->phenotype.volume.divide();
    child->set_total_volume(child->phenotype.volume.total);
    set_total_volume(phenotype.volume.total);

    // child->set_phenotype( phenotype );
    child->phenotype = phenotype;

    return child;
}

void EvoNanoCell::changeCellType(Cell *cell, Cell_Definition *templateCellDefinition) {
    if(templateCellDefinition != nullptr){
        cell->convert_to_cell_definition(*templateCellDefinition);
    }
}

void EvoNanoCell::deleteCell(Cell *cell) {
    delete_cell(cell);
    delete cell;
    cell = nullptr;
}

EvoNanoCell::EvoNanoCell(Cell *cellToCloneFrom, Cell_Definition *templateCellDefinition) {
    if( templateCellDefinition != nullptr) {
        convert_to_cell_definition(*templateCellDefinition);
    } else if(cellToCloneFrom != nullptr){
        this->copy_data(cellToCloneFrom);
        this->copy_function_pointers(cellToCloneFrom);
        this->parameters = parameters;
    }

    (*all_cells).push_back( this );
    this->index=(*all_cells).size()-1;

    // new usability enhancements in May 2017

    if( BioFVM::get_default_microenvironment() )
    {
        this->register_microenvironment( BioFVM::get_default_microenvironment() );
    }
    // All the phenotype and other data structures are already set
    // by virtue of the default Cell constructor.
}
EvoNanoCell::EvoNanoCell() : Cell() {}
EvoNanoCell::~EvoNanoCell() {
    deleteCell(this);
}