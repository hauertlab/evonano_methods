//
// Created by Mihailo Vasiljevic on 2019-05-27.
//

#ifndef EVO_NANO_CELLBUILDER_H
#define EVO_NANO_CELLBUILDER_H

#include "../../core/PhysiCell.h"
using namespace PhysiCell;

/**
 * @class CellDefinitionBuilder should be able to build desired definition that can the be used by @see { CellFactory.h }
 */
class CellDefinitionBuilder {
protected:
    Cell_Definition *cellDefinition;
public:
    virtual CellDefinitionBuilder *configureCellDefinition() = 0;
    Cell_Definition* build();
    ~CellDefinitionBuilder();
};

class CancerStemCellDefinitionBuilder : public CellDefinitionBuilder{
    CellDefinitionBuilder *configureCellDefinition();
};

class CancerCellDefinitionBuilder : public CellDefinitionBuilder{
    CellDefinitionBuilder *configureCellDefinition();
};

class VascularityCellDefinitionBuilder : public CellDefinitionBuilder{
    CellDefinitionBuilder *configureCellDefinition();
};

#endif //EVO_NANO_CELLBUILDER_H
