//
// Created by Mihailo Vasiljevic on 2019-05-28.
//

#include "Util.h"
#include <cstdlib>
#include <iostream>

bool Util::calculate_probability_for_developing_new_cell(int lower_boundary, int upper_boundary, int percentage_factor, double probability){
    // rand between 0.2-0.8
    int rnd = lower_boundary + ( std::rand() % ( upper_boundary - lower_boundary + 1 ) );
    double percentage = (double)rnd / percentage_factor;
    return percentage >= probability ? true : false;
    //return true;
}

