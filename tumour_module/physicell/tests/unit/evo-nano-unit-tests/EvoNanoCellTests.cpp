//
// Created by Mihailo Vasiljevic on 2019-05-28.
//

#include "../../../custom_modules/cell_factory/EvoNanoCell.h"
#include "../../../custom_modules/cell_factory/EvoNanoCellContainer.h"
#include "../../../core/PhysiCell.h"
#include "../../../modules/PhysiCell_settings.h"
/*
 *
 * Some portions of code should be commented out in order this ad hod tests to work.
 * Comment in EvoNanoCell.cpp divide method:
  if( norm(rand_vec) < 1e-16 )
    {
        std::cout<<"************ERROR********************"<<std::endl;
    }
    normalize( &rand_vec );
    // rand_vec/= norm(rand_vec);
    child->assign_position(position[0] + 0.5 * radius*rand_vec[0],
                           position[1] + 0.5 * radius*rand_vec[1],
                           position[2] + 0.5 * radius*rand_vec[2]);
    //change my position to keep the center of mass intact and then see if I need to update my voxel index
    static double negative_one_half = -0.5;
    naxpy( &position, negative_one_half , rand_vec );// position = position - 0.5*rand_vec;
    // position[0] -= 0.5*radius*rand_vec[0];
    // position[1] -= 0.5*radius*rand_vec[1];
    // position[2] -= 0.5*radius*rand_vec[2];

    update_voxel_in_container();
    phenotype.volume.divide();
    child->phenotype.volume.divide();
    child->set_total_volume(child->phenotype.volume.total);
    set_total_volume(phenotype.volume.total);

    // child->set_phenotype( phenotype );
    child->phenotype = phenotype;

 * and in concrete cell definition builders:
     // Set cell-cell adhesion to 5% of other cells
    cellDefinition->phenotype.mechanics.cell_cell_adhesion_strength *=
            parameters.doubles( "cancer_cell_relative_adhesion" ); // 0.05;

    // Set apoptosis to zero
    cellDefinition->phenotype.death.rates[apoptosis_model_index] =
            parameters.doubles( "cancer_cell_apoptosis_rate" ); // 0.0;

    // Set proliferation to 10% of other cells.
    // Alter the transition rate from G0G1 state to S state
    cellDefinition->phenotype.cycle.data.transition_rate(G0G1_index,S_index) *=
            parameters.doubles( "cancer_cell_relative_cycle_entry_rate" ); // 0.1;
 */

using namespace PhysiCell;
using namespace std;

void loadPhysiCellSettings(){
    bool XML_status = false;
    XML_status = load_PhysiCell_config_file( "../../../config/PhysiCell_settings.xml" );
    if( !XML_status )
    { exit(-1); }
}

void setup_microenvironment( void )
{
    // set domain parameters

/*
	default_microenvironment_options.X_range = {-500, 500};
	default_microenvironment_options.Y_range = {-500, 500};
	default_microenvironment_options.Z_range = {-500, 500};
*/
    // make sure to override and go back to 2D
    if( default_microenvironment_options.simulate_2D == true )
    {
        std::cout << "Warning: overriding XML config option and setting to 3D!" << std::endl;
        default_microenvironment_options.simulate_2D = false;
    }


    // no gradients need for this example

    default_microenvironment_options.calculate_gradients = false;

    // set Dirichlet conditions

    default_microenvironment_options.outer_Dirichlet_conditions = true;

    // if there are more substrates, resize accordingly
    std::vector<double> bc_vector( 1 , 38.0 ); // 5% o2
    default_microenvironment_options.Dirichlet_condition_vector = bc_vector;

    // initialize BioFVM

    initialize_microenvironment();

    return;
}

void setupContainer(Cell_Container* cell_container){
    // PNRG setup
    SeedRandom();

    // time setup
    std::string time_units = "min";

    /* Microenvironment setup */

    setup_microenvironment(); // modify this in the custom code

    /* PhysiCell setup */

    // set mechanics voxel size, and match the data structure to BioFVM
    double mechanics_voxel_size = 30;
    cell_container = create_cell_container_for_microenvironment( microenvironment, mechanics_voxel_size );
}

string cellBuilderCreatingCancerStemCellDefinition(){

    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerStemCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    return cancerStemCellDefinition->type == 2 ? "Yes" : "No";
}

string cellBuilderCreatingCancerCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    return cancerCellDefinition->type == 1 ? "Yes" : "No";
}

string cellFactoryCreatingValidCancerCellFromCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    return pC->type_name.compare("CC") == 0 ? "Yes" : "No";
}

string cellFactoryCreatingValidCancerStemCellFromCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    return pC->type_name.compare("CSC") == 0 ? "Yes" : "No";
}

string cloneEvoNanoCellWithoutCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    Cell *clonedCell = ((EvoNanoCell *)pC)->clone(pC, nullptr);
    return clonedCell->type_name.compare(pC->type_name) == 0 ? "Yes" : "No";
}

string cloneEvoNanoCellWithCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);

    cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    Cell *clonedCell = ((EvoNanoCell *)pC)->clone(pC, cancerCellDefinition);
    return clonedCell->type_name.compare(cancerCellDefinition->name) == 0 ? "Yes" : "No";
}

string cloneEvoNanoCellWithCellDefinitionAndWithoutCellTemplate(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);

    cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    Cell *clonedCell = ((EvoNanoCell *)pC)->clone(nullptr, cancerCellDefinition);
    return clonedCell->type_name.compare(cancerCellDefinition->name) == 0 ? "Yes" : "No";
}

string changeCellTypeWithoutCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    ((EvoNanoCell *)pC)->changeCellType(pC, nullptr);

    return pC->type_name.compare("CSC") == 0 ? "Yes" : "No";
}

string changeCellTypeWithCellDefinition(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);

    cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    ((EvoNanoCell *)pC)->changeCellType(pC, cancerCellDefinition);

    return pC->type_name.compare("CC") == 0 ? "Yes" : "No";
}

string divideIntoTwoCancerCells(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);


    Cell *childCell = ((EvoNanoCell *)pC)->divide();

    return childCell->type_name.compare(pC->type_name) == 0 ? "Yes" : "No";
}

string divideIntoOneCancerAndOneCancerStemCell(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);

    cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    Cell *childCell = ((EvoNanoCell *)pC)->divide(cancerCellDefinition);
    return childCell->type_name.compare(pC->type_name) != 0 ? "Yes" : "No";
}

string divideIntoTwoCancerStemCells(){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);

    cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();

    Cell *childCell = ((EvoNanoCell *)pC)->divide(cancerCellDefinition, cancerCellDefinition);
    return childCell->type_name.compare(pC->type_name) == 0 ? "Yes" : "No";
}

string cellContainerCreateBothCellDefinitions(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    bool probability = true;
    std::map<Cell_Definition *, bool> *definitionMap = new std::map<Cell_Definition *, bool>();
    cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    (*definitionMap)[new Cell_Definition(*cancerCellDefinition)] = probability;
    (*definitionMap)[new Cell_Definition(*cancerCellDefinition)] = probability;
    std::vector<Cell_Definition*> *returned = ((EvoNanoCellContainer *) cellContainer)->createCellDefinitions(definitionMap);

    for(int i = 0; i < returned->size(); i++){
        if((*returned)[i] == nullptr){
            return "No";
        }
    }
    return "Yes";
}

string cellContainerCreateBothCellDefinitionsDifferent(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    bool probability = true;
    std::map<Cell_Definition *, bool> *definitionMap = new std::map<Cell_Definition *, bool>();
    (*definitionMap)[new Cell_Definition(*cancerCellDefinition)] = probability;
    cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    (*definitionMap)[new Cell_Definition(*cancerCellDefinition)] = probability;
    std::vector<Cell_Definition*> *returned = ((EvoNanoCellContainer *) cellContainer)->createCellDefinitions(definitionMap);
    if((*returned)[0]->name.compare("CC") == 0 && (*returned)[1]->name.compare("CSC") == 0) {
        return "Yes";
    } else {
        return "No";
    }
}

string cellContainerCreateOnlyOneCellDefinition(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    bool probability = true;
    std::map<Cell_Definition *, bool> *definitionMap = new std::map<Cell_Definition *, bool>();
    (*definitionMap)[nullptr] = false;
    cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    (*definitionMap)[new Cell_Definition(*cancerCellDefinition)] = probability;
    std::vector<Cell_Definition*> *returned = ((EvoNanoCellContainer *) cellContainer)->createCellDefinitions(definitionMap);
    if((*returned)[0] == nullptr && (*returned)[1]->name.compare("CSC") == 0) {
        return "Yes";
    } else {
        return "No";
    }
}

string cellContainerDivideCancerCellToTwoCancerCells(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    std::vector<bool> probabilities = {false, false};
    std::vector<Cell *> *cellsReadyToDivide = new std::vector<Cell *>();
    cellsReadyToDivide->push_back(pC);
    Cell *childCell = ((EvoNanoCellContainer *) cellContainer)->cellDivision(cellsReadyToDivide, probabilities);
    return childCell->type_name.compare(pC->type_name) == 0 ? "Yes" : "No";
}

string cellContainerDivideCancerCellToOneStemCell(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    std::vector<bool> probabilities = {true, false};
    std::vector<Cell *> *cellsReadyToDivide = new std::vector<Cell *>();
    cellsReadyToDivide->push_back(pC);
    Cell *childCell = ((EvoNanoCellContainer *) cellContainer)->cellDivision(cellsReadyToDivide, probabilities);
    return (childCell->type_name.compare("CSC") == 0 && pC->type_name.compare("CC") == 0) ? "Yes" : "No";
}

string cellContainerDivideCancerCellToTwoStemCell(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    std::vector<bool> probabilities = {true, true};
    std::vector<Cell *> *cellsReadyToDivide = new std::vector<Cell *>();
    cellsReadyToDivide->push_back(pC);
    Cell *childCell = ((EvoNanoCellContainer *) cellContainer)->cellDivision(cellsReadyToDivide, probabilities);
    return (childCell->type_name.compare("CSC") == 0 && pC->type_name.compare("CSC") == 0) ? "Yes" : "No";
}

string cellContainerDivideCancerStemCellToOneCancerCell(Cell_Container *cellContainer){
    CellDefinitionBuilder *cellDefinitionBuilder = new CancerStemCellDefinitionBuilder();
    Cell_Definition *cancerCellDefinition = cellDefinitionBuilder->configureCellDefinition()->build();
    CellFactory *cellFactory = new CellFactory();
    Cell *pC = cellFactory->createCell(nullptr, cancerCellDefinition);
    std::vector<bool> probabilities = {true, false};
    std::vector<Cell *> *cellsReadyToDivide = new std::vector<Cell *>();
    cellsReadyToDivide->push_back(pC);
    Cell *childCell = ((EvoNanoCellContainer *) cellContainer)->cellDivision(cellsReadyToDivide, probabilities);
    return (childCell->type_name.compare("CC") == 0 && pC->type_name.compare("CSC") == 0) ? "Yes" : "No";
}

int main() {
    loadPhysiCellSettings();
    Cell_Container* cell_container;
    setupContainer(cell_container);
    cout << "=========== CD BUILDER AND CELL FACTORY ==============" << endl;
    cout << "Cancer stem cell definition valid: " << cellBuilderCreatingCancerStemCellDefinition() << endl;
    cout << "Cancer cell definition valid: " << cellBuilderCreatingCancerCellDefinition() << endl;
    cout << "Cancer stem cell creation valid: " << cellFactoryCreatingValidCancerStemCellFromCellDefinition() << endl;
    cout << "Cancer cell creation valid: " << cellFactoryCreatingValidCancerCellFromCellDefinition() << endl;
    cout << "=========== !CD BUILDER AND CELL FACTORY ==============" << endl;

    cout << "=========== EVO NANO CELL LOGIC ==============" << endl;
    cout << "Evo nano cell clone without cell definition working as expected: " << cloneEvoNanoCellWithoutCellDefinition() << endl;
    cout << "Evo nano cell clone with cell definition working as expected: " << cloneEvoNanoCellWithCellDefinition() << endl;
    cout << "Evo nano cell clone with cell definition and without cell template working as expected: " << cloneEvoNanoCellWithCellDefinitionAndWithoutCellTemplate() << endl;
    cout << "Evo nano cell change type without cell definition: " << changeCellTypeWithoutCellDefinition() << endl;
    cout << "Evo nano cell change type with cell definition: " << changeCellTypeWithCellDefinition() << endl;
    cout << "Evo nano cell divide into two cancer cells: " << divideIntoTwoCancerCells() << endl;
    cout << "Evo nano cell divide into one cancer cell and one cancer stem cell: " << divideIntoOneCancerAndOneCancerStemCell() << endl;
    cout << "Evo nano cell divide into two cancer stem cells: " << divideIntoTwoCancerStemCells() << endl;
    cout << "Evo nano cell container create both definitions: " << cellContainerCreateBothCellDefinitions(cell_container) << endl;
    cout << "Evo nano cell container create both different definitions: " << cellContainerCreateBothCellDefinitionsDifferent(cell_container) << endl;
    cout << "Evo nano cell container create only one definition: " << cellContainerCreateOnlyOneCellDefinition(cell_container) << endl;
    cout << "Evo nano cell container divide to two cancer cells: " << cellContainerDivideCancerCellToTwoCancerCells(cell_container) << endl;
    cout << "Evo nano cell container divide to one cancer stem cell and one cancer cell: " << cellContainerDivideCancerCellToTwoCancerCells(cell_container) << endl;
    cout << "Evo nano cell container divide to two cancer stem cells: " << cellContainerDivideCancerCellToTwoStemCell(cell_container) << endl;
    cout << "Evo nano cell container divide to one cancer cell and one cancer stem cell: " << cellContainerDivideCancerStemCellToOneCancerCell(cell_container) << endl;

    cout << "=========== !EVO NANO CELL LOGIC ==============" << endl;
}


