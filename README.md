# README #

Current version of the evolved cancer simulator for EVONANO

### Overview ###

There are two example simulations (CC and CSC). Scenarios are as follows: The first assumes that all cells are either cancer cells or ECM and NPs are contained within a well-mixed environment such that spatial distribution is unimportant. The second includes additional cell type of cancer stem cell (CSC). 

* CC  
	This folder contains all the files required to simulate a well-mixed reaction diffusion simulation of NP internalisation into a section of tumour tissue. Simulations are run using the run_sim_optim.py file and optimisation is run using optimize.py. 
* CSC  
	This folder contains all the files required to simulate a well-mixed reaction diffusion simulation of NP internalisation into a section of tumour tissue which include CSC. Simulations are run using the run_sim_optim.py file and optimisation is run using optimize.py.

### Input file ###

Simulation configurations is contained within settings.xml. Details are:

* Category: cell - contains cell types, number of cells, number of receptors (N_R), file_name for output, and generation for evolution
* Category: options - optimise (for optimisation), multi_core (for parallelisation), visualise (for visualising NP diffusion), steps (for number of rounds of evolution), n_individuals (for population size for evolution)
* Category: parameters - containing diffusion const (D), binding coeff (k), input of NPs (NP0), treatment time (T). These are also evolved paramters when optimising
* Category: parameter_range - as parameter but containing ranges for optimisation.

### Running ###

Simulations can be run by changing into relevant folder and running: 

* ./run_sim_optim.py - basic simulation using STEPS
* ./optimize.py - evolution using ranges within config file for simulation running steps. 
